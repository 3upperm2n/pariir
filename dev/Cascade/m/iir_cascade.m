% chebshev (direct form 1) bandpass filter
% coefficients are converted to single section 
% 
% author:   leiming yu
% date:     march 31,2013


clear
clc

load handel.mat
hfile = 'handel.wav';
wavwrite(y, Fs, hfile)

[y, Fs, nbits, readinfo] = wavread(hfile);
y_in = y(1:40000,1);

% use fdatool to load the iir_chebshev_bandpass_sp.fda
% export the numerator and denumerator
num=[0.259900669161358,0,-1.81930468412951,0,5.45791405238852,0,-9.09652342064754,0,9.09652342064754,0,-5.45791405238852,0,1.81930468412951,0,-0.259900669161358;];
den=[1,-2.22101709561834e-15,-4.36277182531099,8.34530459983470e-15,8.53328603977497,-1.17145572716637e-14,-9.53128851947154,9.41683919031916e-15,6.51295470495995,-4.33015684472465e-15,-2.68179288825499,1.20015293125275e-15,0.597928236673033,-1.36732060269026e-16,-0.0472634382083068;];

y_out=filter(num,den, y_in);

save('audio.txt','y_in','-ascii')
save('validation_audio.txt','y_out','-ASCII')