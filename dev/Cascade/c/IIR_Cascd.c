//	IIR Filter (direct form I) , Single Precision
//  By Leiming Yu (ylm@coe.neu.edu) 
//	Last Modified Date: April 10, 2013
//
//  Reference to the book 	"Real Time Digital Signal Processing: 
//                			Implementation and Application, 2nd Ed"
//                			By Sen M. Kuo, Bob H. Lee, and Wenshun Tian
//                			Publisher: John Wiley and Sons, Ltd
//

#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h> // boolean
#include<stdint.h> // uint64_t
#include<time.h>
#include<math.h>
#include"fdacoefs.h"


int64_t timespecDiff(struct timespec *timeA_p, struct timespec *timeB_p)
{
	return ((timeA_p->tv_sec * 1000000000) + timeA_p->tv_nsec) -
		((timeB_p->tv_sec * 1000000000) + timeB_p->tv_nsec);
}

int main(int argc, char *argv[])
{
	if(argc == 1){
		printf("Error. Specify audio input file!\n");
		exit(1);
	}

	// read audio data
	unsigned int lineNum=0;
	char buffer[BUFSIZ];
	FILE *fr = fopen(argv[1], "r");
	if(fr == NULL) {
		printf("Can't open audio.txt!");
	}else{
		while(fgets(buffer, sizeof(buffer), fr))
		{
			lineNum++;
		}
		fclose(fr);
	}


	float *inputArray,*outputArray,*vald;
	inputArray  = (float*) malloc(sizeof(float)*lineNum);
	outputArray = (float*) malloc(sizeof(float)*lineNum);
	vald 		= (float*) malloc(sizeof(float)*lineNum);

	// read input
	lineNum = 0;
	fr = fopen(argv[1], "r");
	if(fr == NULL) {
		printf("Can't open audio.txt!");
	}else{
		while(fgets(buffer, sizeof(buffer), fr))
		{
			fscanf(fr,"%f",&inputArray[lineNum]); // read single precision input
			lineNum++;
		}
		fclose(fr);
	}

	// IIR Filtering 
	unsigned int i,j;

	float *x,*y;
	float zx,zy;
	x = (float *) malloc(sizeof(float)*NL);
	y = (float *) malloc(sizeof(float)*DL);

	for(i=0;i<NL;i++)	x[i]=0.f;
	for(i=0;i<DL;i++)	y[i]=0.f;

	struct timespec start,end;

	clock_gettime(CLOCK_MONOTONIC, &start); // compile with -lrt

	for(i=0; i<lineNum; i++)
	{
		// shift input signal in time domain
		for(j=NL-1; j>0; j--){
			x[j] = x[j-1];
		}

		x[0] = inputArray[i];

		// multiply with the numerator coefficients
		zx = 0.f;
		for(j=0; j<NL; j++){
			zx += x[j]*NUM[j];
		}

		// update delayed output 
		for(j=DL-1; j>0; j--){
			y[j] = y[j-1];
		}
		zy = 0.f;
		for(j=1; j<DL; j++){
			zy += y[j]*DEN[j];
		}

		y[0] = zx - zy;
		outputArray[i] = y[0];

	}

	clock_gettime(CLOCK_MONOTONIC, &end);
	uint64_t timeElapsed = timespecDiff(&end,&start);

	printf("Execution time: %f second.\n", timeElapsed/(float)1000000000);

	// Validation: 
	lineNum = 0;
	fr = fopen("validation_audio.txt", "r");
	if(fr == NULL) {
		printf("Can't open validation_audio.txt!\n");
	}else{
		while(fgets(buffer, sizeof(buffer), fr))
		{
			fscanf(fr,"%f",&vald[lineNum]);
			lineNum++;
		}
		fclose(fr);
	}

	bool same = true;
	for(i=0; (i<lineNum) && same; i++){
		if( abs(outputArray[i] - vald[i]) >= 0.001){
			printf("Wrong data: line %d\n",i);
			printf("C: %.10f   Matlab: %.10f\n", outputArray[i], vald[i]);
			same = false;
		}
	}
	if(same)
		printf("C output matches Matlab output. Succeed!\n");
	else
		printf("C output mis-matches Matlab output. Check again!\n");

	// release host memory
	free(x);
	free(y);
	free(inputArray); 
	free(outputArray); 
	free(vald); 


	return 0;
}


