#pragma OPENCL EXTENSION cl_amd_printf : enable
__kernel void iir_z1(  __global float *x, __global float *z1, __global float *num,  const unsigned int n, const unsigned int nl)
{                                          
	// nl = 15, halo is appened at the left most 
	// 14(halo elements) + 256
	__local float sm_data[270];

	const size_t gid  = get_global_id(0);
	const size_t lid  = get_local_id(0);
	const size_t gpid = get_group_id(0);
	const size_t ls   = get_local_size(0);

	// no need to consider the right most halo (tail)
	if(gid<n){
		if(lid<14){
			sm_data[lid] = ( (gpid==0)? 0.f : x[gpid*ls-14+lid] );
			//printf("id %d; sm= %f \n",gid, sm_data[lid])
		}
		sm_data[14+lid] = x[gid];
	}

	barrier(CLK_LOCAL_MEM_FENCE);

	if(gid<n){
		double tmp=0.0;
		int m;
		for(m=0; m<nl; m++){
			tmp += sm_data[lid+m] * num[14-m];
		}
		// can use shared memory to increase write coherence
		z1[gid] = convert_float(tmp);
	}

}

