//  IIR Filter (direct form I) , Single Precision
//  By Leiming Yu (ylm@coe.neu.edu) 
//  Last Modified Date: April 10, 2013
//
//  Reference to the book   "Real Time Digital Signal Processing: 
//                          Implementation and Application, 2nd Ed"
//                          By Sen M. Kuo, Bob H. Lee, and Wenshun Tian
//                          Publisher: John Wiley and Sons, Ltd
//


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <CL/opencl.h>
#include <sys/stat.h>

#include "fdacoefs.h"

#define OCL_CHECK(x) oclcheck((x),__FILE__,__LINE__)

int64_t timespecDiff(struct timespec *timeA_p, struct timespec *timeB_p)
{
	return ((timeA_p->tv_sec * 1000000000) + timeA_p->tv_nsec) -
		((timeB_p->tv_sec * 1000000000) + timeB_p->tv_nsec);
}

void oclcheck(int err, const char *file, const int linenum)
{
	if(err != CL_SUCCESS){
		printf("Error! file %s: line %d", file, linenum);	
		printf("\n");
		exit(-1);
	}
}

int main( int argc, char* argv[] )
{
	if(argc == 1){
		printf("Error. Specify audio input file!\n");
		exit(1);
	}

	unsigned int i,j,lineNum;

	// Host vectors
	float *h_a;
	float *h_num;
	float *h_z1;
	float *outputArray;
	float *vald;
	float zy;

	// Device buffers
	cl_mem d_a;
	cl_mem d_num;
	cl_mem d_z1;

	cl_platform_id cpPlatform;        // OpenCL platform
	cl_device_id device_id;           // device ID
	cl_context context;               // context
	cl_command_queue queue;           // command queue
	cl_program program;               // program
	cl_kernel kernel;                 // kernel

	cl_ulong start; 	// nano second
	cl_ulong end;
	cl_event event;
	float gpuTime;

	// cpu timer
	struct timespec start_cputimer, end_cputimer;
	float cpuTime;


	float *y;
	y =(float*)calloc(DL,sizeof(float));

	// check the audio length
	FILE *fin;
	char line[100];
	lineNum=0;
	fin = fopen(argv[1],"r"); 
	while(fgets(line,100,fin) != NULL){
		lineNum++;
	}
	fclose(fin);
	printf("Audio length = %u\n",lineNum);

	unsigned int n = lineNum;

	size_t bytes = n*sizeof(float);
	size_t numSize = NL*sizeof(float);

	// Allocate memory for each vector on host
	h_a			= (float*)malloc(bytes);
	h_num 		= (float*)malloc(numSize);
	h_z1 		= (float*)malloc(bytes);
	outputArray	= (float*)malloc(bytes);
	vald 		= (float*)malloc(bytes);

	// read audio data
	lineNum=0;
	fin = fopen(argv[1],"r"); 
	while(fgets(line,100,fin) != NULL){
		sscanf(line,"%f",&h_a[lineNum]);
		lineNum++;
	}
	fclose(fin);


	// Initialize vectors on host
	for( i = 0; i<n; i++ ){
		h_z1[i] = 0.f;
	}

	for( i = 0; i<NL; i++ ){
		h_num[i] = NUM[i]; 
	}


	// opencl works on the first part

	size_t globalSize, localSize;
	cl_int err;

	// Number of work items in each local work group
	localSize = 256;

	// Number of total work items - localSize must be devisor
	globalSize = ceil(n/(float)localSize)*localSize;

	// Bind to platform
	err = clGetPlatformIDs(1, &cpPlatform, NULL);
	OCL_CHECK(err);

	// Get ID for the device
	err = clGetDeviceIDs(cpPlatform, CL_DEVICE_TYPE_GPU, 1, &device_id, NULL);
	OCL_CHECK(err);

	// Create a context  
	context = clCreateContext(0, 1, &device_id, NULL, NULL, &err);
	OCL_CHECK(err);

	// Create a command queue 
	//queue = clCreateCommandQueue(context, device_id, 0, &err);
	queue = clCreateCommandQueue(context, device_id,CL_QUEUE_PROFILING_ENABLE, &err);
	OCL_CHECK(err);

	// Create the compute program from the source buffer
	char *fileName = "kernel.cl";
	FILE *fh = fopen(fileName, "r");
	if(!fh) {
		printf("Error: Failed to open file\n");
		exit(1);
	}
	struct stat statbuf;
	stat(fileName, &statbuf);
	char *kernelSource = (char *) malloc(statbuf.st_size + 1);
	fread(kernelSource, statbuf.st_size, 1, fh);
	kernelSource[statbuf.st_size] = '\0';

	program = clCreateProgramWithSource(context, 1, (const char **) & kernelSource, NULL, &err);
	OCL_CHECK(err);

	// Build the program executable 
	err = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
	OCL_CHECK(err);

	// Create the compute kernel 
	kernel = clCreateKernel(program, "iir_z1", &err);
	OCL_CHECK(err);

	// Create the input and output arrays in device memory for our calculation
	d_a 	= clCreateBuffer(context, CL_MEM_READ_ONLY	, bytes,   NULL, NULL);
	d_num 	= clCreateBuffer(context, CL_MEM_READ_ONLY	, numSize, NULL, NULL);
	d_z1 	= clCreateBuffer(context, CL_MEM_READ_WRITE	, bytes,   NULL, NULL);

	// Write our data set into the input array in device memory
	err = clEnqueueWriteBuffer(queue, d_a, CL_TRUE, 0, bytes, h_a, 0, NULL, NULL);
	OCL_CHECK(err);

	err = clEnqueueWriteBuffer(queue, d_num, CL_TRUE, 0, numSize, h_num, 0, NULL, NULL);
	OCL_CHECK(err);

	err = clEnqueueWriteBuffer(queue, d_z1, CL_TRUE, 0, bytes, h_z1, 0, NULL, NULL);
	OCL_CHECK(err);

	// Set the arguments to our compute kernel
	err = clSetKernelArg(kernel, 0, sizeof(cl_mem), &d_a);
	OCL_CHECK(err);

	err = clSetKernelArg(kernel, 1, sizeof(cl_mem), &d_z1);
	OCL_CHECK(err);

	err = clSetKernelArg(kernel, 2, sizeof(cl_mem), &d_num);
	OCL_CHECK(err);

	err = clSetKernelArg(kernel, 3, sizeof(unsigned int), &n);
	OCL_CHECK(err);

	err = clSetKernelArg(kernel, 4, sizeof(unsigned int), &NL);
	OCL_CHECK(err);

	// Execute the kernel over the entire range of the data set  
	// err = clEnqueueNDRangeKernel(queue, kernel, 1, NULL, &globalSize, &localSize, 0, NULL, NULL);
	err = clEnqueueNDRangeKernel(queue, kernel, 1, NULL, &globalSize, &localSize, 0, NULL, &event);
	OCL_CHECK(err);

	// Wait for the command queue to get serviced before reading back results
	clFinish(queue);

	// Read the results from the device
	err = clEnqueueReadBuffer(queue, d_z1, CL_TRUE, 0, bytes, h_z1, 0, NULL, NULL );
	OCL_CHECK(err);

	err = clWaitForEvents(1,&event);
	OCL_CHECK(err);

	// get the start time of execution
	err = clGetEventProfilingInfo (event, CL_PROFILING_COMMAND_START, sizeof(cl_ulong), &start, NULL);
	OCL_CHECK(err);

	// get the time of execution finish
	err = clGetEventProfilingInfo (event, CL_PROFILING_COMMAND_END, sizeof(cl_ulong), &end, NULL);
	OCL_CHECK(err);


	gpuTime = (float) (end-start)/1000000000.f;
	printf("gpu runtime = %f second \n", gpuTime);


	// second part
	clock_gettime(CLOCK_MONOTONIC, &start_cputimer);

	for(i=0; i<lineNum; i++)
	{
		for(j=DL-1; j>0; j--)		
		{
			y[j]=y[j-1]; 
		}
		zy = 0.f; 
		for(j=1;j<DL; j++)
		{
			zy += y[j]*DEN[j];
		}

		y[0] = h_z1[i] - zy;
		outputArray[i] = y[0];
	}

	clock_gettime(CLOCK_MONOTONIC, &end_cputimer);
	cpuTime = timespecDiff(&end_cputimer,&start_cputimer)/(float)1000000000;

	printf("cpu runtime = %f second \n", cpuTime);

	// Validation: 	validation_audio.txt
	//				check the first 8 digits after the decimal
	lineNum=0;
	fin = fopen("validation_audio.txt","r"); 
	while(fgets(line,100,fin) != NULL){
		sscanf(line,"%f",&vald[lineNum]);
		lineNum++;
	}
	fclose(fin);

	bool same = true;
	for(i=0; (i<lineNum) && same; i++){
		if( fabs(outputArray[i] - vald[i]) >= 0.001){
			printf("Wrong data: line %u\n",i);
			printf("C: %15.10f Matlab: %15.10f\n", outputArray[i], vald[i]);
			same = false;
		}
	}
	if(same)
		printf("C output matches Matlab output. Succeed!\n");
	else
		printf("C output mis-matches Matlab output. Check again!\n");


	// release OpenCL resources
	clReleaseMemObject(d_a);
	clReleaseMemObject(d_num);
	clReleaseMemObject(d_z1);

	clReleaseProgram(program);
	clReleaseKernel(kernel);
	clReleaseCommandQueue(queue);
	clReleaseContext(context);

	//release host memory
	free(h_a);
	free(h_num);
	free(h_z1);
	free(outputArray);
	free(vald);

	return 0;
}
