//	IIR Filter Parallelized using SIMD 
//  By Leiming Yu (ylm@coe.neu.edu) 

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/time.h>


typedef float v4sf __attribute__ ((vector_size (16))); // vector of four single floats

typedef union f4vector {
	v4sf v;
	float f[4];
} f4vector_t;

v4sf broadcast(float a){
	return (v4sf){a,a,a,a};
}

unsigned long getTimeOfDay() {
	struct timeval tv;
	gettimeofday(&tv, NULL);
	return tv.tv_sec * 1000000.0 + tv.tv_usec;
}

void canonical_iir(unsigned int len);
void simd_iir(unsigned int len);


int main(int argc, char *argv[])
{
	if(argc != 2){
		printf("Wrong Usage!\nTo use program, type like $./PROGRAM DATA_LENGTH.\n");
		exit(1);
	}

	unsigned int N = atol(argv[1]);

	printf("=>Start Program...\n");

	printf("\nCanonical IIR\n");
	canonical_iir(N);

	printf("\nParallelize IIR using SIMD extensions\n");
	simd_iir(N);

	printf("\n=>End\n");

	return 0;
}


void canonical_iir(unsigned int len)
{
	unsigned int i,j,N=len;
	unsigned long start_timer, end_timer;
	double systemTime;

	float *x,*y;
	x = (float*) malloc(sizeof(float)*N);
	y = (float*) malloc(sizeof(float)*N);
	// input
	for(i=0;i<N;++i){
		x[i] = 0.01f;
	}

	// IIR Filtering 
	int NL=3,DL=NL-1;	// nominator/denominator coefficients

	float *nom,*denom;
	nom = (float *) malloc(sizeof(float)*NL);
	denom = (float *) malloc(sizeof(float)*DL);
	//printf("\nnominator:\n");   
	for(i=0;i<NL;++i){
		nom[i] = 0.1f;
		//printf("%f\n",nom[i]);
	}
	//printf("\ndenominator:\n");   
	// assume a0 = 1, others = 0.2f
	for(j=0;j<DL;++j){
		denom[j] = 0.2f;
		//printf("%f\n",denom[j]);
	}

	float *buffer_x, *buffer_y;
	buffer_x = (float *) malloc(sizeof(float)*NL);
	buffer_y = (float *) malloc(sizeof(float)*DL);
	memset(buffer_x,0,sizeof(float)*NL);
	memset(buffer_y,0,sizeof(float)*DL);
	//printf("\nbuffer x:\n");
	//for(i=0;i<NL;++i){
	//	printf("%f\n",buffer_x[i]);
	//}
	//printf("\nbuffer y:\n");
	//for(i=0;i<DL;++i){
	//	printf("%f\n",buffer_y[i]);
	//}

	double sum_x;	
	double sum_y;	
	
	start_timer = getTimeOfDay();
	//iir filtering
	for(i=0;i<N;++i){
		// shift data in buffer x	
		for(j=NL-1;j>=1;--j){
			buffer_x[j] = buffer_x[j-1];	
		}

		buffer_x[0] = x[i];

		// multiply the nominaotr coef.
		sum_x = 0.0;
		for(j=0;j<NL;++j){
			sum_x += buffer_x[j]*nom[j];	
		}

		// shift data in buffer y	
		for(j=DL-1;j>=1;--j){
			buffer_y[j] = buffer_y[j-1];	
		}

		// multiply the denominaotr coef.
		sum_y = 0.0;
		for(j=0;j<DL;++j){
			sum_y += buffer_y[j]*denom[j];	
		}
		y[i] = buffer_y[0] = (float)(sum_x - sum_y);
	}

	end_timer = getTimeOfDay();
	systemTime = (double)(end_timer - start_timer) / 1000000.0;
	printf("Canonical-IIR Elapsed Time(second) = %lf\n", systemTime);

	//for(i=0;i<N;++i){
	//	printf("%f\n",y[i]);
	//}


	//
	free(x);
	free(y);
	free(nom);
	free(denom);
	free(buffer_x);
	free(buffer_y);



	return;
}


void simd_iir(unsigned int len)
{

	unsigned int i,j,N=len/4;
	unsigned long start_timer, end_timer;
	double systemTime;

	f4vector_t *x,*y;
	x = (f4vector_t*) malloc(sizeof(f4vector_t)*N);
	y = (f4vector_t*) malloc(sizeof(f4vector_t)*N);

	// input
	for(i=0;i<N;++i){
		x[i].v = (v4sf){0.01f,0.01f,0.01f,0.01f};
	}

	int NL=3,DL=NL-1;	// nominator/denominator coefficients

	float *nom,*denom;
	nom = (float *) malloc(sizeof(float)*NL);
	denom = (float *) malloc(sizeof(float)*DL);
	for(i=0;i<NL;++i){
		nom[i] = 0.1f;
	}
	for(j=0;j<DL;++j){
		denom[j] = 0.2f;
	}

	// coefficients	
	float coeff[4][8]=
	{
		{0.f,	0.f,	0.f,	0.1f,	0.1f,	0.1f,	0.2f,	0.2f},
		{0.f,	0.f,	0.1f,	0.1f,	0.1f,	0.f,	0.2f,	0.f},
		{0.f,	0.1f,	0.1f,	0.1f,	0.f,	0.f,	0.f,	0.f},
		{0.1f,	0.1f,	0.1f,	0.f,	0.f,	0.f,	0.f,	0.f},
	};

	// update
	for(i=0;i<8;++i){
		coeff[1][i] += denom[0]*coeff[0][i];
		coeff[2][i] += denom[0]*coeff[1][i] + denom[1]*coeff[0][i];
		coeff[3][i] += denom[0]*coeff[2][i] + denom[1]*coeff[1][i];
	}
	
	// SIMD coefficients
	f4vector_t coeff_xp3, coeff_xp2, coeff_xp1, coeff_x0, coeff_xm1, coeff_xm2, coeff_ym1, coeff_ym2;

	coeff_xp3.v = (v4sf){coeff[0][0], coeff[1][0], coeff[2][0], coeff[3][0]};  
	coeff_xp2.v = (v4sf){coeff[0][1], coeff[1][1], coeff[2][1], coeff[3][1]};  
	coeff_xp1.v = (v4sf){coeff[0][2], coeff[1][2], coeff[2][2], coeff[3][2]};  
	coeff_x0.v  = (v4sf){coeff[0][3], coeff[1][3], coeff[2][3], coeff[3][3]};  
	coeff_xm1.v = (v4sf){coeff[0][4], coeff[1][4], coeff[2][4], coeff[3][4]};  
	coeff_xm2.v = (v4sf){coeff[0][5], coeff[1][5], coeff[2][5], coeff[3][5]};  
	coeff_ym1.v = (v4sf){coeff[0][6], coeff[1][6], coeff[2][6], coeff[3][6]};  
	coeff_ym2.v = (v4sf){coeff[0][7], coeff[1][7], coeff[2][7], coeff[3][7]};  


	f4vector_t xm2, xm1, ym2, ym1; 
	f4vector_t x0, xp1, xp2, xp3; 
	f4vector_t y0123, y0123_a, y0123_b;

	xm2.v = broadcast(0.f);
	xm1.v = broadcast(0.f);
	ym2.v = broadcast(0.f);
	ym1.v = broadcast(0.f);

	start_timer = getTimeOfDay();

	for(i=0; i<N; ++i){
		// new input 
		x0.v  = broadcast(x[i].f[0]);
		xp1.v = broadcast(x[i].f[1]);
		xp2.v = broadcast(x[i].f[2]);
		xp3.v = broadcast(x[i].f[3]);
	
		// calculate output	
		y0123_a.v = coeff_xp3.v*xp3.v + coeff_xp2.v*xp2.v + coeff_xp1.v*xp1.v + coeff_x0.v*x0.v;
		y0123_b.v = coeff_xm1.v*xm1.v + coeff_xm2.v*xm2.v + coeff_ym1.v*ym1.v + coeff_ym2.v*ym2.v;
		y0123.v = y0123_a.v + y0123_b.v;

		// write output
		y[i].v = y0123.v;

		// update recurrence
		//if(i<N-1){
		xm2.v = xp2.v;
		xm1.v = xp3.v;
		ym2.v = broadcast(y0123.f[2]);
		ym1.v = broadcast(y0123.f[3]);
		//}
	}

	//b.v = (v4sf){2.f,2.f,2.f,2.f};
	//printf("%f, %f, %f, %f\n", c.f[0], c.f[1], c.f[2], c.f[3]);
	end_timer = getTimeOfDay();
	systemTime = (double)(end_timer - start_timer) / 1000000.0;
	printf("SIMD-IIR Elapsed Time(second) = %lf\n", systemTime);


	free(x);
	free(y);
	free(nom);
	free(denom);

	return;
}
