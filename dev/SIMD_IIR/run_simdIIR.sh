#!/bin/bash


# delete the previous record
if [ -f ./profile_opencl/len_$1.txt ];
then
  rm ./profile_opencl/len_$1.txt
fi

for (( i=1 ; i<=100; i = i+1 ))
do
./simd_iir $1 >> ./profile_simd/len_$1.txt 
done

./time_canonical.sh   ./profile_simd/len_$1.txt | cut -f1 | sort -nr | tail -1 >> ./profile_simd/final_data_canonical.txt 
./time_simd.sh        ./profile_simd/len_$1.txt | cut -f1 | sort -nr | tail -1 >> ./profile_simd/final_data_simd_iir.txt 

