#!/bin/bash


# ./profile options, where options can be -ocl [OpenCL version] , clean [remove profiling data]
# Usage:  ./profile -ocl
#         ./clean

# read the command options
if [ $# -eq 0 ];
then
  echo "$0 : You need to specify options, such as -simd or clean."
  exit 1
fi

if [ $# -gt 1 ];
then
  echo "$0 : Too many options. Use -simd or clean."
  exit 1
fi

#-------------------------------
# Remove previous profiling data 
#-------------------------------
if test $1 = "clean";
then
  if [ -d "profile_simd" ];
  then
    rm -rf "profile_simd"
  fi

else

  let "opt = 0"
  #-----------------------
  # profile OpenCL version
  #-----------------------
  if test $1 = "-simd";
  then
    let "opt++"
  
    if [ ! -d "./profile_simd" ];
    then
      mkdir "profile_simd"
    fi
  
    if [ -f ./profile_simd/final_data*.txt ];
    then 
      rm ./profile_simd/final_data*.txt 
    fi

    for (( i = 512 ;  i <= 1048576 ; i = i*2 ))
    do
	  echo "Run data length $i"
      ./run_simdIIR.sh $i
    done

	echo "Check the directory ./profile_simd"
  
  fi
  
  if test $opt -eq 0;
  then
    echo "$0 : Wrong arguments. It should be -simd or clean."
    exit 1
  fi

fi





