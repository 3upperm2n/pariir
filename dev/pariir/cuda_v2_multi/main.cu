// System includes
#include <stdio.h>
#include <assert.h>

// CUDA runtime
#include <cuda_runtime.h>

// helper functions and utilities to work with CUDA
#include <helper_functions.h>
#include <helper_cuda.h>
#include <helper_math.h> 

#include "cuPrintf.cu"

//The macro CUPRINTF is defined for architectures
//with different compute capabilities.
#if __CUDA_ARCH__ < 200     //Compute capability 1.x architectures
#define CUPRINTF cuPrintf
#else                       //Compute capability 2.x architectures
#define CUPRINTF(fmt, ...) printf("[%d, %d]:\t" fmt, \
		blockIdx.y*gridDim.x+blockIdx.x,\
		threadIdx.z*blockDim.x*blockDim.y+threadIdx.y*blockDim.x+threadIdx.x,\
		__VA_ARGS__)
#endif

#define SIZE 8192 
#define ROWS 256  // num of parallel subfilters
#define DEB 0
#define TIMING 1

__constant__ float2 NSEC[ROWS];
__constant__ float2 DSEC[ROWS];

void check(float *cpu, float *gpu, int n);
void cpu_pariir(float *x, float *y, float *ns, float *dsec, float c, int len);


template <int blockSize>
__global__ void GpuParIIR (float *x, int len, float c, float *y)
{
	extern __shared__ float sm[];
	float *sp = &sm[ROWS];

	int tid = threadIdx.x;
	//int id = __mul24(blockIdx.x, blockDim.x) + threadIdx.x;

	// & 0x20
	int lane_id = tid % 32; // warp size 32 for +3.5 device
	int warp_id = tid / 32;

	int ii, jj, kk;
	//int gid;

	float2 u = make_float2(0.0f);
	float unew;
	float y0;

	// block size : ROWS
	// each thread fetch input x to shared memory
	for(ii=0; ii<len; ii+=ROWS)
	{
		sm[tid] = x[tid + ii];	

		__syncthreads();

		// go through each x in shared memory 
		for(jj=0; jj<ROWS; jj++)	
		{
			//gid = ii * ROWS + jj;

			unew = sm[jj] - dot(u, DSEC[tid]);				
			u = make_float2(unew, u.x);
			y0 = dot(u, NSEC[tid]);

			if(jj==0 && ii == 0)
			{
				//CUPRINTF("tid = %d,\t value = %f\n", tid, sm[jj]);
				//CUPRINTF("tid = %d, value = %f\n", tid, unew);
				//CUPRINTF("tid = %d, value = %f\n", tid, y0);
			}

			if(jj==1)
			{
				//CUPRINTF("tid = %d,\t value = %f\n", tid, sm[jj]);
				//CUPRINTF("tid = %d, value = %f\n", tid, unew);
				//CUPRINTF("tid = %d, value = %f\n", tid, y0);
			}

			if(jj==0 && ii == ROWS)
			{
				// 0.1f 
				//CUPRINTF("tid = %d,\t value = %f\n", tid, sm[jj]);
				// 0.099996
				//CUPRINTF("tid = %d, value = %f\n", tid, unew);
				// 0.099996
				//CUPRINTF("tid = %d, u(%f , %f)\n", tid, u.x, u.y);
				// 0.000010 
				//CUPRINTF("tid = %d, value = %f\n", tid, y0);
			}

		
			// sum v across current block
			#pragma unroll
			for(kk=1; kk<32; kk<<=1)  
			{
				y0 += __shfl_xor(y0, kk, 32); 
			}

			if(jj==0)
			{
			//	if(lane_id == 0)
			//		CUPRINTF("tid = %d,\t value = %f\n", tid, y0);
			}

			if(jj==1)
			{
			//	if(lane_id == 0)
			//		CUPRINTF("tid = %d,\t value = %f\n", tid, y0);
			}

			if(jj==0 && ii == ROWS) {
				if(lane_id == 0)
					CUPRINTF("tid = %d,\t value = %f\n", tid, y0);
			}

			if(lane_id == 0)
			{
				sp[warp_id] = y0;
			}

			__syncthreads();

			// 0.000160 

			if(jj==0)
			{
		//		if(lane_id == 0)
		//			CUPRINTF("tid = %d,\t sp = %f\n", tid, sp[warp_id]);
			}
			// 0.000320
			if(jj==1)
			{
//				if(lane_id == 0)
//					CUPRINTF("tid = %d,\t sp = %f\n", tid, sp[warp_id]);
			}

			if(jj==0 && ii == ROWS) {
				// 0.00032
			//	if(lane_id == 0)
			//		CUPRINTF("tid = %d,\t sp = %f\n", tid, sp[warp_id]);
			}

			if(blockSize == 256 && warp_id == 0)
			{
				if(lane_id < 8)
				{
					float warp_sum = sp[lane_id];
					if(jj==0 && ii== ROWS){
						//CUPRINTF("tid = %d,\t value = %f\n", tid, warp_sum);
					}
					if(jj==1){
						// 0.000320
						//CUPRINTF("tid = %d,\t value = %f\n", tid, warp_sum);
					}

					warp_sum += __shfl_xor(warp_sum, 1, 32);  // ? 32
					warp_sum += __shfl_xor(warp_sum, 2, 32);  // ? 32
					warp_sum += __shfl_xor(warp_sum, 4, 32);  // ? 32

					//#pragma unroll
					//for(kk=1; kk<8; kk<<=1)
					//{
					//	warp_sum += __shfl_xor(warp_sum, kk, 8);  // ? 32
					//}

					//if(jj==0){
					if(jj==0 && ii== ROWS){
						// 0.002560
						//CUPRINTF("tid = %d,\t (shfl) = %f\n", tid, warp_sum);
					}

					if(jj==1){
						// 0.00256
						// CUPRINTF("tid = %d,\t (shfl) = %f\n", tid, warp_sum);
					}

					if(lane_id == 0)
					{
						int gid = ii + jj;

						y[gid] = warp_sum + sm[jj] * c;	

						if(jj==0) { // 3 * 0.1 + 0.001280  
							//CUPRINTF("tid = %d,\t y = %f\n", tid, y[gid]);
						}

						if(jj==1) { // 3 * 0.1 + 0.00256
							//CUPRINTF("tid = %d,\t y = %f\n", tid, y[gid]);
							//CUPRINTF("gid = %d,\t y = %f\n", gid, y[gid]);
						}

						if(jj==0 && ii == ROWS) { // 3 * 0.1 + 0.002560 
							// CUPRINTF("gid = %d,\t y = %f\n", gid, y[gid]);
						}

						//CUPRINTF("gid = %d,\t y = %f\n", gid, y[gid]);
					}
				}

			}
		}
	
	}

}

__global__ void testKernel(int val)
{
	CUPRINTF("\tValue is:%d\n", val);
}


int main(int argc, char *argv[])
{
	if(argc != 2){
		printf("Specify the lenght of input, e.g. ./main len !\n");
		exit(1);
	}

	int i, j;
	int len = atoi(argv[1]); // num of elements
	int channels = 1;

	size_t bytes = sizeof(float) * len;

	// input
	float *x= (float*) malloc(bytes);
	for (i=0; i<len; i++){
		x[i] = 0.1f;
	}

	// output: multi-channel
	float *gpu_y= (float*) malloc(bytes * channels);

	// cpu output:
	float *cpu_y= (float*) malloc(bytes);

	float c = 3.0;

	// coefficients
	float *nsec, *dsec;
	nsec = (float*) malloc(sizeof(float) * 2 * ROWS); // numerator
	dsec = (float*) malloc(sizeof(float) * 3 * ROWS); // denominator

	for(i=0; i<ROWS; i++){
		for(j=0; j<3; j++){
			dsec[i*3 + j] = 0.00002f;
		}
	}

	for(i=0; i<ROWS; i++){
		for(j=0; j<2; j++){
			nsec[i*2 + j] = 0.00005f;
		}
	}

	cpu_pariir(x, cpu_y, nsec, dsec, c, len);

	int warpsize = 32;
	int warpnum = ROWS/warpsize;

	// vectorize the coefficients
	float2 *vns, *vds;
	vns = (float2*) malloc(sizeof(float2) * ROWS);
	vds = (float2*) malloc(sizeof(float2) * ROWS); 

	for(i=0; i<ROWS; i++){
		vds[i] = make_float2(0.00002f);
		vns[i] = make_float2(0.00005f);
	}

	// timer
	cudaEvent_t start, stop;

	// device memory
	float *d_x;
	cudaMalloc((void **)&d_x, bytes);

	float *d_y;
	cudaMalloc((void **)&d_y, bytes * channels);

	// copy data to constant memory
	cudaMemcpyToSymbol(NSEC, vns, sizeof(float2)*ROWS, 0, cudaMemcpyHostToDevice);
	cudaMemcpyToSymbol(DSEC, vds, sizeof(float2)*ROWS, 0, cudaMemcpyHostToDevice);
	cudaMemcpy(d_x, x, bytes, cudaMemcpyHostToDevice);

	if(TIMING)
	{
		cudaEventCreate(&start);
		cudaEventCreate(&stop);

		// start timer
		cudaEventRecord(start, 0);
	}

	// kernel
	GpuParIIR <ROWS> <<< channels , ROWS, sizeof(float) * (ROWS + warpnum) >>>(d_x, len, c, d_y);

	if(TIMING)
	{
		// end timer
		cudaEventRecord(stop, 0);
		cudaEventSynchronize(stop);

		float et;
		cudaEventElapsedTime(&et, start, stop);
		printf ("ElapsetTime = %f (s)\n", et/1000.f);
	}


	cudaMemcpy(gpu_y, d_y, bytes * channels, cudaMemcpyDeviceToHost);

	check(cpu_y, gpu_y, len);

	// release
	cudaFree(d_x);
	cudaFree(d_y);

	free(x);
	free(gpu_y);
	free(cpu_y);
	free(dsec);
	free(nsec);
	free(vds);
	free(vns);

}

void check(float *cpu, float *gpu, int n)
{
	int i;
	int success = 1;
	for(i=0; i<n; i++)
	{
		if(cpu[i] - gpu[i] > 0.0001)	
		{
			puts("Failed!");
			success = 0;
			break;
		}
	}

	if(success)
		puts("Passed!");

	if(DEB)
	{
		for(i=0; i<n; i++)
		{
			printf("[%d]\t cpu=%f \t gpu=%f\n", i, cpu[i], gpu[i]);	
		}
	}
}

void cpu_pariir(float *x, float *y, float *ns, float *dsec, float c, int len)
{
	int i, j;
	float out;
	float unew;

	float *ds = (float*) malloc(sizeof(float) * ROWS * 2);	

	// internal state
	float *u = (float*) malloc(sizeof(float) * ROWS * 2);
	memset(u, 0 , sizeof(float) * ROWS * 2);

	for(i=0; i<ROWS; i++)
	{
		ds[i * 2]     = dsec[3 * i + 1];
		ds[i * 2 + 1] = dsec[3 * i + 2];
	}

	for(i=0; i<len; i++)
	{
		out = c * x[i];

		for(j=0; j<ROWS; j++)
		{
			unew = x[i] - (ds[j*2] * u[j*2] + ds[j*2+1] * u[j*2+1]);
			u[j*2+1] = u[j * 2];
			u[j*2] = unew;
			out = out + (u[j*2] * ns[j*2] + u[j*2 + 1] * ns[j*2 + 1]);
		}

		y[i] = out;
	}

	free(ds);
	free(u);
}
