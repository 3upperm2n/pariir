// System includes
#include <stdio.h>
#include <assert.h>
#include <time.h>
#include <sys/time.h>

// CUDA runtime
#include <cuda_runtime.h>

// helper functions and utilities to work with CUDA
#include <helper_functions.h>
#include <helper_cuda.h>
#include <helper_math.h> 

#include "cuPrintf.cu"

//The macro CUPRINTF is defined for architectures
//with different compute capabilities.
#if __CUDA_ARCH__ < 200     //Compute capability 1.x architectures
#define CUPRINTF cuPrintf
#else                       //Compute capability 2.x architectures
#define CUPRINTF(fmt, ...) printf("[%d, %d]:\t" fmt, \
		blockIdx.y*gridDim.x+blockIdx.x,\
		threadIdx.z*blockDim.x*blockDim.y+threadIdx.y*blockDim.x+threadIdx.x,\
		__VA_ARGS__)
#endif

#define SIZE 8192 
#define ROWS 256  // num of parallel subfilters
#define DEB 0
#define TIMING 1

__constant__ float2 NSEC[ROWS];
__constant__ float2 DSEC[ROWS];

void check(float *cpu, float *gpu, int len, int tot_chn);
void cpu_pariir(float *x, float *y, float *ns, float *dsec, float c, int len, int channels);
void tic(struct timeval *timer);
void toc(struct timeval *timer);
void need_argument(int argc, char **argv, int argi);


template <int blockSize>
__global__ void GpuParIIR (float *x, int len, float c, float *y)
{
	// shared memory layout:
	// ROWS(input x) + ROWS(temporary) 
	extern __shared__ float sm[];
	float *sp = &sm[ROWS];

	int tid = threadIdx.x;
	//int id = __mul24(blockIdx.x, blockDim.x) + threadIdx.x;

	// & 0x20
	//int lane_id = tid % 32; // warp size 32 for +3.5 device
	//int warp_id = tid / 32;

	int ii, jj;

	float2 u = make_float2(0.0f);
	float unew;
	//float y0;

	// block size : ROWS
	// each thread fetch input x to shared memory
	for(ii=0; ii<len; ii+=ROWS)
	{
		sm[tid] = x[tid + ii];	

		__syncthreads();

		// go through each x in shared memory 
		for(jj=0; jj<ROWS; jj++)	
		{
			unew = sm[jj] - dot(u, DSEC[tid]);				
			u = make_float2(unew, u.x);
			//y0 = dot(u, NSEC[tid]);

			// save to shared memory pointed by sp
			sp[tid]= dot(u, NSEC[tid]);

			__syncthreads();
		
			// sum y0 in current block (parallel reduction) 
			if (blockDim.x >= 512)
			{
				if (tid < 256)
					sp[tid] += sp[tid + 256];

				__syncthreads();
			}

			if (blockDim.x >= 256)
			{
				if (tid < 128)
					sp[tid] += sp[tid + 128];

				__syncthreads();
			}

			if (blockDim.x >= 128)
			{
				if (tid < 64)
					sp[tid] += sp[tid + 64];

				__syncthreads();
			}


			if (tid < 32)
			{
				// now that we are using warp-synchronous programming (below)
				// we need to declare our shared memory volatile so that the compiler
				// doesn't reorder stores to it and induce incorrect behavior.
				volatile float *smem = sp;

				if (blockDim.x >=  64)
					smem[tid] += smem[tid + 32];

				if (blockDim.x >=  32)
					smem[tid] += smem[tid + 16];

				if (blockDim.x >=  16)
					smem[tid] += smem[tid +  8];

				if (blockDim.x >=   8)
					smem[tid] += smem[tid +  4];

				if (blockDim.x >=   4)
					smem[tid] += smem[tid +  2];

				if (blockDim.x >=   2)
					smem[tid] += smem[tid +  1];
			}

			if (tid == 0)
			{
				//int gid = ii + jj;
				// channel starting postion: blockId.x * len
				uint gid = __mul24(blockIdx.x , len) + ii + jj;
				y[gid] = sp[0] + sm[jj] * c;	
			}

			__syncthreads();

		} // end of jj loop
	} // end of input loop
}// end of kernel

__global__ void testKernel(int val)
{
	CUPRINTF("\tValue is:%d\n", val);
}


int main(int argc, char *argv[])
{
	int i, j;
	int len, channels;

	/*
	 * Parse Command line
	 */
	if (argc == 1)
	{
		len = 1024; // ( multiple of ROWS )
		channels = 64;
	}
	else if(argc == 5)
	{
		int argi;
		for (argi = 1; argi < argc; argi++)
		{
			if (!strcmp(argv[argi], "-l")){
				need_argument(argc, argv, argi);
				len = atoi(argv[++argi]);      
			}
			else if (!strcmp(argv[argi], "-c")) {
				need_argument(argc, argv, argi);
				channels = atoi(argv[++argi]);      
			}else{
				printf("%s is not a valid command-line option.\n\n", argv[argi]); 
				exit (EXIT_FAILURE);
			}
		}
	}
	else
	{
		printf("Usage: ./Program -l signal_length -c channel_number!\n");	
		exit (EXIT_FAILURE);
	}


	printf("\nSignal Length = %d", len);
	printf("\nNumber of Channels = %d\n", channels);

	size_t bytes = sizeof(float) * len;

	// input
	float *x= (float*) malloc(bytes);
	for (i=0; i<len; i++){
		x[i] = 0.1f;
	}

	// output: multi-channel
	float *gpu_y= (float*) malloc(bytes * channels);

	// cpu output:
	float *cpu_y= (float*) malloc(bytes);

	float c = 3.0;

	// coefficients
	float *nsec, *dsec;
	nsec = (float*) malloc(sizeof(float) * 2 * ROWS); // numerator
	dsec = (float*) malloc(sizeof(float) * 3 * ROWS); // denominator

	for(i=0; i<ROWS; i++){
		for(j=0; j<3; j++){
			dsec[i*3 + j] = 0.00002f;
		}
	}

	for(i=0; i<ROWS; i++){
		for(j=0; j<2; j++){
			nsec[i*2 + j] = 0.00005f;
		}
	}

	printf("\n");

	/*
	 * CPU Implementation
	 */
	cpu_pariir(x, cpu_y, nsec, dsec, c, len, channels);

	//int warpsize = 32;
	//int warpnum = ROWS/warpsize;

	// vectorize the coefficients
	float2 *vns, *vds;
	vns = (float2*) malloc(sizeof(float2) * ROWS);
	vds = (float2*) malloc(sizeof(float2) * ROWS); 

	for(i=0; i<ROWS; i++){
		vds[i] = make_float2(0.00002f);
		vns[i] = make_float2(0.00005f);
	}


	printf("\n");

	/* 
	 * Query for platforms/devices
	 */
	int DeviceNum = 0;
	cudaError_t error_id = cudaGetDeviceCount(&DeviceNum);
	if (error_id != cudaSuccess)
	{
		printf("cudaGetDeviceCount returned %d\n-> %s\n", (int)error_id, cudaGetErrorString(error_id));
		printf("Result = FAIL\n");
		exit(EXIT_FAILURE);
	}

	if (DeviceNum == 0)
		printf("There are no available device(s) that support CUDA\n");
	else
		printf("Detected %d CUDA Capable device(s)\n", DeviceNum);

	cudaDeviceProp *devProp = (cudaDeviceProp*)malloc(sizeof(cudaDeviceProp) * DeviceNum);
	for (i=0; i<DeviceNum; i++)
	{
		cudaSetDevice(i);	
		cudaGetDeviceProperties(&devProp[i], i);
		printf("Device %d: \"%s\"\n", i, devProp[i].name);
		printf("  CUDA Capability Major/Minor version number:    %d.%d\n", 
				devProp[i].major, devProp[i].minor);
		printf("  Total amount of global memory:                 %.0f MBytes\n",
				(float)devProp[i].totalGlobalMem/1048576.0f);
		printf("  (%2d) Multiprocessors, (%3d) CUDA Cores/MP:     %d CUDA Cores\n",
				devProp[i].multiProcessorCount,
				_ConvertSMVer2Cores(devProp[i].major, devProp[i].minor),
				_ConvertSMVer2Cores(devProp[i].major, devProp[i].minor) * devProp[i].multiProcessorCount);
	}


	int target = 0;
	cudaSetDevice(target);
	printf("\n=>Choose Device %d: \"%s\"\n\n", target, devProp[target].name);


	// timer
	cudaEvent_t start, stop;

	// device memory
	float *d_x;
	cudaMalloc((void **)&d_x, bytes);

	float *d_y;
	cudaMalloc((void **)&d_y, bytes * channels);


	// copy data to constant memory
	cudaMemcpyToSymbol(NSEC, vns, sizeof(float2)*ROWS, 0, cudaMemcpyHostToDevice);
	cudaMemcpyToSymbol(DSEC, vds, sizeof(float2)*ROWS, 0, cudaMemcpyHostToDevice);
	cudaMemcpy(d_x, x, bytes, cudaMemcpyHostToDevice);

	// warmup for better timing
//	for(i=0; i<200; i++)
//		GpuParIIR <ROWS> <<< channels , ROWS, sizeof(float) * (ROWS * 2) >>>(d_x, len, c, d_y);

	if(TIMING)
	{
		cudaEventCreate(&start);
		cudaEventCreate(&stop);

		// start timer
		cudaEventRecord(start, 0);
	}

	// kernel : using shared memory
	GpuParIIR <ROWS> <<< channels , ROWS, sizeof(float) * (ROWS * 2) >>>(d_x, len, c, d_y);

	if(TIMING)
	{
		// end timer
		cudaEventRecord(stop, 0);
		cudaEventSynchronize(stop);

		float et;
		cudaEventElapsedTime(&et, start, stop);
		printf ("GPU Time = %f (s)\n", et/1000.f);
	}

	cudaMemcpy(gpu_y, d_y, bytes * channels, cudaMemcpyDeviceToHost);


	check(cpu_y, gpu_y, len, channels);

	// release
	cudaFree(d_x);
	cudaFree(d_y);

	free(devProp);
	free(x);
	free(gpu_y);
	free(cpu_y);
	free(dsec);
	free(nsec);
	free(vds);
	free(vns);
}

void check(float *cpu, float *gpu, int len, int tot_chn)
{
	int i;
	int chn;
	uint start;
	int success = 1;

	
	for(chn=0; chn<tot_chn; chn++)
	{
		start = chn * len;

		for(i=0; i<len; i++)
		{
			if(cpu[i] - gpu[i + start] > 0.0001)	
			{
				puts("Failed!");
				success = 0;
				break;
			}
		}
	}

	if(success)
		puts("\nPassed!\n");

	if(DEB)
	{
		for(i=0; i<len; i++)
		{
			printf("[%d]\t cpu=%f \t gpu=%f\n", i, cpu[i], gpu[i]);	
		}
	}
}

void cpu_pariir(float *x, float *y, float *ns, float *dsec, float c, int len, int channels)
{
	int i, j, k;
	float out;
	float unew;

	float *ds = (float*) malloc(sizeof(float) * ROWS * 2);	

	// internal state
	float *u = (float*) malloc(sizeof(float) * ROWS * 2);

	for(i=0; i<ROWS; i++)
	{
		ds[i * 2]     = dsec[3 * i + 1];
		ds[i * 2 + 1] = dsec[3 * i + 2];
	}

	struct timeval timer;
	tic(&timer);

	// multiple channels simulation
	for(k=0; k<channels; k++)
	{
		memset(u, 0 , sizeof(float) * ROWS * 2);

		for(i=0; i<len; i++)
		{
			out = c * x[i];

			for(j=0; j<ROWS; j++)
			{
				unew = x[i] - (ds[j*2] * u[j*2] + ds[j*2+1] * u[j*2+1]);
				u[j*2+1] = u[j * 2];
				u[j*2] = unew;
				out = out + (u[j*2] * ns[j*2] + u[j*2 + 1] * ns[j*2 + 1]);
			}

			y[i] = out;
		}
	}

	toc(&timer);

	free(ds);
	free(u);
}

void tic(struct timeval *timer)
{
	gettimeofday(timer, NULL);
}


void toc(struct timeval *timer)
{
	struct timeval tv_end;
	struct timeval tv_diff;
	long int diff;

	gettimeofday(&tv_end, NULL);

	diff = (tv_end.tv_usec + 1000000 * tv_end.tv_sec) - (timer->tv_usec + 1000000 * timer->tv_sec);

	tv_diff.tv_sec  = diff / 1000000;
	tv_diff.tv_usec = diff % 1000000;

	printf("CPU Time = %ld.%06ld (s)\n", tv_diff.tv_sec, tv_diff.tv_usec);
}

void need_argument(int argc, char **argv, int argi)
{
	if (argi == argc - 1)
	{
		printf("option %s requires one argument.\n\n", argv[argi]);
		exit (EXIT_FAILURE);
	}
}
