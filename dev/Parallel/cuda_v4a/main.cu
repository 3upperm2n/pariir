/*
	MiroBenchmark for SHFL instruction
	Reduction
*/

// System includes
#include <stdio.h>
#include <assert.h>

// CUDA runtime
#include <cuda_runtime.h>

// helper functions and utilities to work with CUDA
#include <helper_functions.h>
#include <helper_cuda.h>

#include "../../common/cpp_utils.h"

#define SIZE 8192 

__constant__ float input[SIZE];

// row
__constant__ float2 const_nsec[32];
__constant__ float2 const_dsec[32];


// http://www.icmc.usp.br/pessoas/castelo/CUDA/common/inc/cutil_math.h
inline __host__  __device__ 
float2 operator*(float2 a, float2 b)
{
	return make_float2(a.x * b.x, a.y * b.y);
}


//__global__ void gpuIIR (float *d_y, float *d_x, int len, float c)
__global__ void gpuIIR (float *d_y, int len, float c)
{
	int tid = threadIdx.x; // 16
	int i;
	int j;

	int lane_id = tid & 0x1F;
	// int warp_id =  tid / 32;

	float x,  x1, x2, x3; 
	float x4, x5, x6, x7; 

	float unew,  unew1, unew2, unew3;
	float unew4, unew5, unew6, unew7;

	float smem,  smem1, smem2, smem3;
	float smem4, smem5, smem6, smem7;

	float2 tmp,  tmp1, tmp2, tmp3;
	float2 tmp4, tmp5, tmp6, tmp7;

	float2 u = make_float2(0.f, 0.f);


	float2 dsec, nsec;
	dsec = const_dsec[tid]; 
	nsec = const_nsec[tid]; 

	for(i=0; i<len ; i += 8)
	{

		x  = input[i];
		x1 = input[i + 1];
		x2 = input[i + 2];
		x3 = input[i + 3];

		x4 = input[i + 4];
		x5 = input[i + 5];
		x6 = input[i + 6];
		x7 = input[i + 7];

		// pipelining
		// 1st stage
		tmp = dsec * u;
		unew = x - (tmp.x + tmp.y);
		u = make_float2(unew, u.x);
		tmp = nsec * u;

		// 2nd stage
		tmp1 = dsec * u;
		unew1 = x1 - (tmp1.x + tmp1.y);
		u = make_float2(unew1, u.x);
		tmp1 = nsec * u;

		// 3rd stage
		tmp2 = dsec * u;
		unew2 = x2 - (tmp2.x + tmp2.y);
		u = make_float2(unew2, u.x);
		tmp2 = nsec * u;
		
		// 4th stage
		tmp3 = dsec * u;
		unew3 = x3 - (tmp3.x + tmp3.y);
		u = make_float2(unew3, u.x);
		tmp3 = nsec * u;

		// 5th stage
		tmp4 = dsec * u;
		unew4 = x4 - (tmp4.x + tmp4.y);
		u = make_float2(unew4, u.x);
		tmp4 = nsec * u;

		// 6th 
		tmp5 = dsec * u;
		unew5 = x5 - (tmp5.x + tmp5.y);
		u = make_float2(unew5, u.x);
		tmp5 = nsec * u;

		// 7th 
		tmp6 = dsec * u;
		unew6 = x6 - (tmp6.x + tmp6.y);
		u = make_float2(unew6, u.x);
		tmp6 = nsec * u;

		// 8th 
		tmp7 = dsec * u;
		unew7 = x7 - (tmp7.x + tmp7.y);
		u = make_float2(unew7, u.x);
		tmp7 = nsec * u;



		smem  = tmp.x  + tmp.y;	
		smem1 = tmp1.x + tmp1.y;	
		smem2 = tmp2.x + tmp2.y;	
		smem3 = tmp3.x + tmp3.y;	

		smem4 = tmp4.x + tmp4.y;	
		smem5 = tmp5.x + tmp5.y;	
		smem6 = tmp6.x + tmp6.y;	
		smem7 = tmp7.x + tmp7.y;	

		for (j=1; j<32; j <<=1 ) {
			smem  += __shfl_xor(smem,  j, 32);
			smem1 += __shfl_xor(smem1, j, 32);
			smem2 += __shfl_xor(smem2, j, 32);
			smem3 += __shfl_xor(smem3, j, 32);

			smem4 += __shfl_xor(smem4, j, 32);
			smem5 += __shfl_xor(smem5, j, 32);
			smem6 += __shfl_xor(smem6, j, 32);
			smem7 += __shfl_xor(smem7, j, 32);
		}
		
		if(lane_id == 0){
			d_y[i]   = smem  + c * x;
			d_y[i+1] = smem1 + c * x1;
			d_y[i+2] = smem2 + c * x2;
			d_y[i+3] = smem3 + c * x3;

			d_y[i+4] = smem4 + c * x4;
			d_y[i+5] = smem5 + c * x5;
			d_y[i+6] = smem6 + c * x6;
			d_y[i+7] = smem7 + c * x7;
		}

	}


}



int main(int argc, char *argv[])
{
	if(argc != 2){
		printf("Specify the lenght of input, e.g. ./main len !\n");
		exit(1);
	}

	int len = atoi(argv[1]); // num of elements

	size_t bytes = sizeof(float) * len;
	float *x= (float*) malloc(bytes);
	int i;
	for (i=0; i<len; i++){
		x[i] = 1.f;
	}

	float *y= (float*) malloc(bytes);

	float c = 3.0;

	int row = 32;

	float2 *dsec = (float2 *) malloc(sizeof(float2)*row);
	for(i=0;i<row;i++){
		dsec[i] = make_float2(0.5f,0.5f);
	}

	float2 *nsec = (float2 *) malloc(sizeof(float2)*row);
	for(i=0;i<row;i++){
		nsec[i] = make_float2(0.2f,0.2f);
	}

	// device memory
	float *d_y;
	cudaMalloc((void **)&d_y, sizeof(float) * len);

	
	// copy data to constant memory
	cudaMemcpyToSymbol(input, x, sizeof(float)*len, 0,  cudaMemcpyHostToDevice);
	cudaMemcpyToSymbol(const_dsec, dsec, sizeof(float2)*row, 0, cudaMemcpyHostToDevice);
	cudaMemcpyToSymbol(const_nsec, nsec, sizeof(float2)*row, 0, cudaMemcpyHostToDevice);

	// start timer
	cudaEvent_t start, stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);

	cudaEventRecord(start, 0);

	// kernel 
	gpuIIR <<< 1, 32 >>>(d_y, len, c);

	// end timer
	cudaEventRecord(stop, 0);
	cudaEventSynchronize(stop);

	float et;
	cudaEventElapsedTime(&et, start, stop);
	printf ("ElapsetTime = %f (s)\n", et/1000.f);

	cudaMemcpy(y, d_y, sizeof(float) * len, cudaMemcpyDeviceToHost);

	//print1df(y, len);

	cudaFree(d_y);

	free(x);
	free(y);
	free(dsec);
	free(nsec);
}
