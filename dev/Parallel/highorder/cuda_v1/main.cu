// System includes
#include <stdio.h>
#include <assert.h>

// CUDA runtime
#include <cuda_runtime.h>

// helper functions and utilities to work with CUDA
#include <helper_functions.h>
#include <helper_cuda.h>
#include <helper_math.h> // floats

#include "../../../common/cpp_utils.h"

#define SIZE 8192 

__constant__ float Input[SIZE];
__constant__ float4 COEF[8];

void form_coeffMatrix(float *coeff, int row, int col, float *nsec, float *dsec, int order);
void update_coeffMatrix(float *coeff, int row, int col, float *dsec, int order);
void print_coeffMatrix(float *coeff, int row, int col);

__global__ void gpuIIR (float *d_y, int len, float c)
{
	//int gix = threadIdx.x + blockIdx.x * blockDim.x;
	unsigned int offset = blockIdx.x * len;
	int warp_id =  threadIdx.x / 32;
	int lane_id = threadIdx.x & 0x1f; 

	__shared__ float sm[2];
	__shared__ float sm1[2];
	__shared__ float sm2[2];
	__shared__ float sm3[2];

	__shared__ float sm02[2];
	__shared__ float sm12[2];
	__shared__ float sm22[2];
	__shared__ float sm32[2];

	__shared__ float sm03[2];
	__shared__ float sm13[2];
	__shared__ float sm23[2];
	__shared__ float sm33[2];

	__shared__ float sm04[2];
	__shared__ float sm14[2];
	__shared__ float sm24[2];
	__shared__ float sm34[2];

	// 2nd order system
	// read previous data xp1 , xp2, xp3
	// current data x0
	// past data xm1, xm2
	// initialize past output y data ym1, ym2
	float4 xm1, xm2, ym1, ym2;

	float4 x0, xp1, xp2, xp3;
	float4 x0_2, xp1_2, xp2_2, xp3_2;
	float4 x0_3, xp1_3, xp2_3, xp3_3;
	float4 x0_4, xp1_4, xp2_4, xp3_4;

	// coefficients 4 x 8 matrix
	float4 coeff_xp3 = COEF[0]; // column 1
	float4 coeff_xp2 = COEF[1];
	float4 coeff_xp1 = COEF[2];
	float4 coeff_x0  = COEF[3];
	float4 coeff_xm1 = COEF[4];
	float4 coeff_xm2 = COEF[5];
	float4 coeff_ym1 = COEF[6];
	float4 coeff_ym2 = COEF[7]; // column 8

	// output y
	float4 y;

	float4 y_a, y_b;
	float4 y_a_2, y_b_2;
	float4 y_a_3, y_b_3;
	float4 y_a_4, y_b_4;

	float y0, y1, y2, y3;
	float y0_2, y1_2, y2_2, y3_2;
	float y0_3, y1_3, y2_3, y3_3;
	float y0_4, y1_4, y2_4, y3_4;

	// initialize
	xm1 = make_float4(0.f);	
	xm2 = make_float4(0.f);	
	ym1 = make_float4(0.f);	
	ym2 = make_float4(0.f);	

	// 4 x 4
	for(int ii=0; ii<len; ii+=16)
	{
		y = make_float4(0.f);

		// each thread read several input
		x0  = make_float4(Input[ii]);
		xp1 = make_float4(Input[ii+1]);
		xp2 = make_float4(Input[ii+2]);
		xp3 = make_float4(Input[ii+3]);
		// 2nd
                x0_2  = make_float4(Input[ii+4]);
                xp1_2 = make_float4(Input[ii+5]);
                xp2_2 = make_float4(Input[ii+6]);
                xp3_2 = make_float4(Input[ii+7]);
		// 3
                x0_3  = make_float4(Input[ii+8]);
                xp1_3 = make_float4(Input[ii+9]);
                xp2_3 = make_float4(Input[ii+10]);
                xp3_3 = make_float4(Input[ii+11]);
		// 4
                x0_4  = make_float4(Input[ii+12]);
                xp1_4 = make_float4(Input[ii+13]);
                xp2_4 = make_float4(Input[ii+14]);
                xp3_4 = make_float4(Input[ii+15]);


		y_a = coeff_xp3 * xp3 
		    + coeff_xp2 * xp2
		    + coeff_xp1 * xp1
		    + coeff_x0  * x0;

		y_b = coeff_xm1 * xm1
		    + coeff_xm2 * xm2
		    + coeff_ym1 * ym1
		    + coeff_ym2 * ym2;

		// 2nd
		y_a_2 = coeff_xp3 * xp3_2 
		      + coeff_xp2 * xp2_2
		      + coeff_xp1 * xp1_2
		      + coeff_x0  * x0_2;
		// 3
		y_a_3 = coeff_xp3 * xp3_3 
		      + coeff_xp2 * xp2_3
		      + coeff_xp1 * xp1_3
		      + coeff_x0  * x0_3;
		// 4
		y_a_4 = coeff_xp3 * xp3_4 
		      + coeff_xp2 * xp2_4
		      + coeff_xp1 * xp1_4
		      + coeff_x0  * x0_4;

		y = y_a + y_b;

		// save temporal output
		y0 = y.x;
		y1 = y.y;
		y2 = y.z;
		y3 = y.w;
		// update for next SIMD operation
		xm2 = xp2;
		xm1 = xp3;
		ym2 = make_float4(y2);
		ym1 = make_float4(y3);
		// 2nd
		y_b_2 = coeff_xm1 * xm1
		      + coeff_xm2 * xm2
		      + coeff_ym1 * ym1
		      + coeff_ym2 * ym2;

		y = y_a_2 + y_b_2;
		// save temporal output
		y0_2 = y.x;
		y1_2 = y.y;
		y2_2 = y.z;
		y3_2 = y.w;
		// update for next loop 
		xm2 = xp2_2;
		xm1 = xp3_2;
		ym2 = make_float4(y2_2);
		ym1 = make_float4(y3_2);
		// 3 
		y_b_3 = coeff_xm1 * xm1
		      + coeff_xm2 * xm2
		      + coeff_ym1 * ym1
		      + coeff_ym2 * ym2;

		y = y_a_3 + y_b_3;
		// save temporal output
		y0_3 = y.x;
		y1_3 = y.y;
		y2_3 = y.z;
		y3_3 = y.w;
		// update for next loop 
		xm2 = xp2_3;
		xm1 = xp3_3;
		ym2 = make_float4(y2_3);
		ym1 = make_float4(y3_3);
		// 4 
		y_b_4 = coeff_xm1 * xm1
		      + coeff_xm2 * xm2
		      + coeff_ym1 * ym1
		      + coeff_ym2 * ym2;

		y = y_a_4 + y_b_4;
		// save temporal output
		y0_4 = y.x;
		y1_4 = y.y;
		y2_4 = y.z;
		y3_4 = y.w;
		// update for next loop 
		xm2 = xp2_4;
		xm1 = xp3_4;
		ym2 = make_float4(y2_4);
		ym1 = make_float4(y3_4);


		// block threads reduction
		for(int j = 1; j < 32; j<<=1) 
		{
			y0 += __shfl_xor(y0, j, 32); 
			y1 += __shfl_xor(y1, j, 32); 
			y2 += __shfl_xor(y2, j, 32); 
			y3 += __shfl_xor(y3, j, 32); 

			y0_2 += __shfl_xor(y0_2, j, 32); 
			y1_2 += __shfl_xor(y1_2, j, 32); 
			y2_2 += __shfl_xor(y2_2, j, 32); 
			y3_2 += __shfl_xor(y3_2, j, 32); 

			y0_3 += __shfl_xor(y0_3, j, 32); 
			y1_3 += __shfl_xor(y1_3, j, 32); 
			y2_3 += __shfl_xor(y2_3, j, 32); 
			y3_3 += __shfl_xor(y3_3, j, 32); 

			y0_4 += __shfl_xor(y0_4, j, 32); 
			y1_4 += __shfl_xor(y1_4, j, 32); 
			y2_4 += __shfl_xor(y2_4, j, 32); 
			y3_4 += __shfl_xor(y3_4, j, 32); 
		}

		if ( lane_id == 31 ) {
			sm[warp_id]  = y0;
			sm1[warp_id]  = y1;
			sm2[warp_id]  = y2;
			sm3[warp_id]  = y3;

			
			sm02[warp_id]  = y0_2;
			sm12[warp_id]  = y1_2;
			sm22[warp_id]  = y2_2;
			sm32[warp_id]  = y3_2;

			sm03[warp_id]  = y0_3;
			sm13[warp_id]  = y1_3;
			sm23[warp_id]  = y2_3;
			sm33[warp_id]  = y3_3;

			sm04[warp_id]  = y0_4;
			sm14[warp_id]  = y1_4;
			sm24[warp_id]  = y2_4;
			sm34[warp_id]  = y3_4;
		}

		__syncthreads();
		
		// 1st thread write out samples
		if(threadIdx.x == 0)
		{
			y0 = sm[0]  + sm[1]; 
			y1 = sm1[0] + sm1[1];
			y2 = sm2[0] + sm2[1];
			y3 = sm3[0] + sm3[1];

			d_y[offset +ii]     = y0 + c * Input[ii];
			d_y[offset +ii + 1] = y1 + c * Input[ii + 1];
			d_y[offset +ii + 2] = y2 + c * Input[ii + 2];
			d_y[offset +ii + 3] = y3 + c * Input[ii + 3];

			y0_2 = sm02[0] + sm02[1];
			y1_2 = sm12[0] + sm12[1];
			y2_2 = sm22[0] + sm22[1];
			y3_2 = sm32[0] + sm32[1];

			d_y[offset +ii + 4] = y0_2 + c * Input[ii + 4];
			d_y[offset +ii + 5] = y1_2 + c * Input[ii + 5];
			d_y[offset +ii + 6] = y2_2 + c * Input[ii + 6];
			d_y[offset +ii + 7] = y3_2 + c * Input[ii + 7];

			y0_3 = sm03[0] + sm03[1];
			y1_3 = sm13[0] + sm13[1];
			y2_3 = sm23[0] + sm23[1];
			y3_3 = sm33[0] + sm33[1];

			d_y[offset +ii + 8] = y0_3 + c * Input[ii + 8];
			d_y[offset +ii + 9] = y1_3 + c * Input[ii + 9];
			d_y[offset +ii +10] = y2_3 + c * Input[ii +10];
			d_y[offset +ii +11] = y3_3 + c * Input[ii +11];

			y0_4 = sm04[0] + sm04[1];
			y1_4 = sm14[0] + sm14[1];
			y2_4 = sm24[0] + sm24[1];
			y3_4 = sm34[0] + sm34[1];

			d_y[offset +ii +12] = y0_4 + c * Input[ii +12];
			d_y[offset +ii +13] = y1_4 + c * Input[ii +13];
			d_y[offset +ii +14] = y2_4 + c * Input[ii +14];
			d_y[offset +ii +15] = y3_4 + c * Input[ii +15];
		}

	}

}


int main(int argc, char *argv[])
{
	if(argc != 2){
		printf("Specify the lenght of input, e.g. ./main len !\n");
		exit(1);
	}

	int len = atoi(argv[1]); // num of elements
	int i;
	int chn = 1;

	size_t bytes = sizeof(float) * len;

	// output
	float *y= (float*) malloc(bytes *chn);

	// input
	float *x= (float*) malloc(bytes);
	for (i=0; i<len; i++){
		x[i] = 0.1f;
	}

	float c = 3.0;

	// coefficients
	int order = 2;
	float *nsec, *dsec;
	nsec = (float*) malloc(sizeof(float) * (order + 1)); // numerator
	dsec = (float*) malloc(sizeof(float) * order); // denominator

	for(i=0; i<(order+1); i++){
		nsec[i] = 0.00002f;
	}
	for(i=0; i<order; i++){
		dsec[i] = 0.00005f;
	}

	// Coefficient  Maxtrix : 4 x 8
	int row = order * 2;
        int col = row * 2;
        float *coeff; // row x col
        coeff = (float*) malloc (sizeof(float) * row * col);
        memset(coeff, 0, sizeof(float) * row * col);

        form_coeffMatrix(coeff, row, col, nsec, dsec, order);
        //print_coeffMatrix(coeff, row, col);

        update_coeffMatrix(coeff, row, col, dsec, order);
        //print_coeffMatrix(coeff, row, col);

	float4 vcoef[8];
	vcoef[0] = make_float4(coeff[0], coeff[8],  coeff[16], coeff[24]);
	vcoef[1] = make_float4(coeff[1], coeff[9],  coeff[17], coeff[25]);
	vcoef[2] = make_float4(coeff[2], coeff[10], coeff[18], coeff[26]);
	vcoef[3] = make_float4(coeff[3], coeff[11], coeff[19], coeff[27]);
	vcoef[4] = make_float4(coeff[4], coeff[12], coeff[20], coeff[28]);
	vcoef[5] = make_float4(coeff[5], coeff[13], coeff[21], coeff[29]);
	vcoef[6] = make_float4(coeff[6], coeff[14], coeff[22], coeff[30]);
	vcoef[7] = make_float4(coeff[7], coeff[15], coeff[23], coeff[31]);


	// device memory
	float *d_y;
	cudaMalloc((void **)&d_y, bytes * chn);

	// copy data to constant memory
	cudaMemcpyToSymbol(Input, x, bytes, 0,  cudaMemcpyHostToDevice);
	cudaMemcpyToSymbol(COEF, vcoef, sizeof(float4)*col, 0, cudaMemcpyHostToDevice);


	dim3 gridSize(chn, 1, 1);
	dim3 blockSize(64, 1, 1);

	// timer
	cudaEvent_t start, stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);

	// start timer
	cudaEventRecord(start, 0);

	// kernel 
	gpuIIR <<< gridSize, blockSize >>>(d_y, len, c);

	// end timer
	cudaEventRecord(stop, 0);
	cudaEventSynchronize(stop);

	float et;
	cudaEventElapsedTime(&et, start, stop);
	printf ("ElapsetTime = %f (s)\n", et/1000.f);

	cudaMemcpy(y, d_y, bytes * chn, cudaMemcpyDeviceToHost);
	//print1df(y, len * chn);

	// release
	cudaFree(d_y);

	free(x);
	free(y);
	free(dsec);
	free(nsec);
	free(coeff);

}

void form_coeffMatrix(float *coeff, int row, int col, float *nsec, float *dsec, int order)
{
	int i, j, k;
	int start, end, width;
	width = order + 1 + order;
	for(i=0; i<row; i++){
		start = col - width - i;
		end = start + width;
		k = 0;
		for(j=start; j<end; j++){
			if(k <(order + 1)){
				coeff[i * col + j] = nsec[k]; // numerator
			}else{
				coeff[i * col + j] = dsec[k-(order+1)];// denominator
			}
			k = k + 1;
		}
	}
}

void print_coeffMatrix(float *coeff, int row, int col)
{
	int i, j;
	for(i=0; i<row; i++){
		for(j=0; j<col; j++){
			printf("%f\t", coeff[i*col + j]);
		}
		printf("\n");
	}
}


void update_coeffMatrix(float *coeff, int row, int col, float *dsec, int order)
{
	int i, ii;
	int j, k;
	float sum;
	for(ii=0; ii<col; ii++){ // each col
		// from 2nd row
		for(i=1; i<row; i++){
			// current row, how many coefficients need to be added?
			if(i<order){
				j = i;
			}else{
				j = order;
			}
			sum = 0.f;
			// add extra coefficients
			for(k=0; k<j; k++) {
				sum += dsec[k] * coeff[(i-1)*col + ii];
			}
			coeff[i*col + ii] += sum;
		}
	}
}





