/*
	MiroBenchmark for SHFL instruction
	Reduction
*/

// System includes
#include <stdio.h>
#include <assert.h>

// CUDA runtime
#include <cuda_runtime.h>

// helper functions and utilities to work with CUDA
#include <helper_functions.h>
#include <helper_cuda.h>

#include "../../common/cpp_utils.h"

#define SIZE 8192 

__constant__ float input[SIZE];
__constant__ float2 const_nsec[16];
__constant__ float2 const_dsec[16];


// http://www.icmc.usp.br/pessoas/castelo/CUDA/common/inc/cutil_math.h
inline __host__  __device__ 
float2 operator*(float2 a, float2 b)
{
	return make_float2(a.x * b.x, a.y * b.y);
}


//__global__ void gpuIIR (float *d_y, float *d_x, int len, float c)
__global__ void gpuIIR (float *d_y, int len, float c)
{
	int tid = threadIdx.x; // 16
	int i;
	int j;

	int lane_id = tid & 0x1F;
	// int warp_id =  tid / 32;

	float x, x1; 
	float unew, unew1;
	float smem, smem1;

	float2 u = make_float2(0.f, 0.f);
	float2 tmp, tmp1;

	float2 dsec, nsec;
	dsec = const_dsec[tid]; 
	nsec = const_nsec[tid]; 

	for(i=0; i<len ; i += 2)
	{

		x  = input[i];
		x1 = input[i + 1];

		// pipelining
		// 1st stage
		tmp = dsec * u;
		unew = x - (tmp.x + tmp.y);
		u = make_float2(unew, u.x);

		tmp = nsec * u;

		// 2nd stage
		tmp1 = dsec * u;
		unew1 = x1 - (tmp1.x + tmp1.y);
		u = make_float2(unew1, u.x);

		tmp1 = nsec * u;

		smem  = tmp.x  + tmp.y;	
		smem1 = tmp1.x + tmp1.y;	

		for (j=1; j<16; j <<=1 ) {
			smem  += __shfl_xor(smem,  j, 32);
			smem1 += __shfl_xor(smem1, j, 32);
		}
		
		if(lane_id == 0){
			d_y[i]   = smem  + c * x;
			d_y[i+1] = smem1 + c * x1;
		}
		//if(lane_id == 0){
		//	//d_y[i] = smem + c * x;
		//	d_y[i] = x;
		//}
	}


}



int main(int argc, char *argv[])
{
	if(argc != 2){
		printf("Specify the lenght of input, e.g. ./main len !\n");
		exit(1);
	}

	int len = atoi(argv[1]); // num of elements

	size_t bytes = sizeof(float) * len;
	float *x= (float*) malloc(bytes);
	int i;
	for (i=0; i<len; i++){
		x[i] = 1.f;
	}

	float *y= (float*) malloc(bytes);

	float c = 3.0;

	int row = 16;

	float2 *dsec = (float2 *) malloc(sizeof(float2)*row);
	for(i=0;i<row;i++){
		dsec[i] = make_float2(0.5f,0.5f);
	}

	float2 *nsec = (float2 *) malloc(sizeof(float2)*row);
	for(i=0;i<row;i++){
		nsec[i] = make_float2(0.2f,0.2f);
	}

	// device memory
	float *d_y;
	cudaMalloc((void **)&d_y, sizeof(float) * len);

	
	// copy data to constant memory
	cudaMemcpyToSymbol(input, x, sizeof(float)*len, 0,  cudaMemcpyHostToDevice);
	cudaMemcpyToSymbol(const_dsec, dsec, sizeof(float2)*row, 0, cudaMemcpyHostToDevice);
	cudaMemcpyToSymbol(const_nsec, nsec, sizeof(float2)*row, 0, cudaMemcpyHostToDevice);

	// start timer
	cudaEvent_t start, stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);

	cudaEventRecord(start, 0);

	// kernel 
	gpuIIR <<< 1, 16 >>>(d_y, len, c);

	// end timer
	cudaEventRecord(stop, 0);
	cudaEventSynchronize(stop);

	float et;
	cudaEventElapsedTime(&et, start, stop);
	printf ("ElapsetTime = %f (s)\n", et/1000.f);




	cudaMemcpy(y, d_y, sizeof(float) * len, cudaMemcpyDeviceToHost);

	//print1df(y, len);

	cudaFree(d_y);

	free(x);
	free(y);
	free(dsec);
	free(nsec);
}
