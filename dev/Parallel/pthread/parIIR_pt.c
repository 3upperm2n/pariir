/*
	Scheme:
		launch 16 threads to produce intemediate data
		using N threads to do reduction on the intermediate data 16 x inputlength
*/

#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h> 
#include<math.h>
#include<time.h>
#include<sys/time.h> // gettimeofday()
#include<pthread.h>
#include<string.h> // stderror

#define NUM_THREADS 16 

//int64_t timespecDiff(struct timespec *timeA_p, struct timespec *timeB_p)
//{
//	return ((timeA_p->tv_sec * 1000000000) + timeA_p->tv_nsec) - 
//		   ((timeB_p->tv_sec * 1000000000) + timeB_p->tv_nsec);
//}


typedef struct
{
	unsigned int len; // input legnth
	float *norm;
	float *denorm;
	float *inputArray; // input x NUM_THREADS
	float *outArray;
} PARFORM;

typedef struct
{
	float c;
	float *X;
	float *input;
	float *output;
	unsigned int length;
} RDTINFO;

PARFORM parform;
RDTINFO rdtinfo;

void *reduction(void *arg)
{
	int taskid;
	taskid = (int) arg;

	// divide the N into NUM_THREADS slices
	float *in, *out, *X;
	in = rdtinfo.input;
	out = rdtinfo.output;
	X = rdtinfo.X;

	float c;
	c = rdtinfo.c;

	unsigned int Len; // N is multiples of 64
	Len = rdtinfo.length;

	unsigned int workPerThread;
	workPerThread = Len/NUM_THREADS;  // N/16

	unsigned int start;
	start = taskid * workPerThread; 

	unsigned int i, pos;
	for(i=0;i<workPerThread;++i){
		pos = start + i;
		out[pos] = in[pos]        + in[Len+pos]    + in[2*Len+pos]  + in[3*Len+pos]  + 
                   in[4*Len+pos]  + in[5*Len+pos]  + in[6*Len+pos]  + in[7*Len+pos]  + 	
                   in[8*Len+pos]  + in[9*Len+pos]  + in[10*Len+pos] + in[11*Len+pos] + 	
                   in[12*Len+pos] + in[13*Len+pos] + in[14*Len+pos] + in[15*Len+pos] + c*X[pos]; 	
	}

	pthread_exit(NULL);

}



void *par_iir(void *arg)
{
	float u[2]={0.f,0.f};

	int taskid;
	taskid = (int) arg;

    unsigned int Len;
	Len = parform.len;

	unsigned int start;
	start = taskid*Len;

	float *nsec, *dsec;
	nsec = parform.norm;
	dsec = parform.denorm;

	float *X,*Y;
	X    = parform.inputArray;
	Y    = parform.outArray;

	// Perform IIR Filtering
	unsigned int i;
	float tmp;
	for(i=0;i<Len;++i)
	{
		tmp = X[start+i] - u[0]*dsec[taskid*2] - u[1]*dsec[taskid*2+1];	
		u[1]=u[0];	
		u[0]=tmp;	
		// update output array
		Y[start+i] = u[0]*nsec[taskid*2] + u[1]*nsec[taskid*2+1];
	}

	pthread_exit(NULL);
}









int main(int argc, char *argv[])
{
	if(argc!=2){
		printf("Wrong Usage!\nTo use program, type like $./PROGRAM DATA_LENGTH.\n");
		exit(1);
	}

	// another timer
	struct timeval start,end;
	gettimeofday(&start,NULL);

	//---------------------
	//	setup Parameters
	//---------------------
	unsigned int i;
	unsigned int Len;
	int tid;
	Len = atol(argv[1]); // input length

	// nominator: 16 rows x 2 columns(second-order)
	int nr,nl;
	nr = 16;
	nl = 2;
	float *nsec;
	nsec = (float*) malloc(sizeof(float)*nr*nl);
	for(i=0;i<32;i++){
		nsec[i] = 0.5;
	}

	// denominator
	int dnr,dnl;
	dnr =16;
	dnl=2;
	float *dsec;
	dsec = (float*) malloc(sizeof(float)*dnr*dnl);
	for(i=0;i<32;i++){
		dsec[i] = 2.0;
	}

	float c = 3.0; // residue

	float *Xarray; // input
	Xarray = (float*) malloc(sizeof(float)*Len*NUM_THREADS);
	for(i=0;i<Len*NUM_THREADS;i++){
		Xarray[i] = 1.0;
	}

	float *Yarray; 
	Yarray  = (float*)calloc(Len*NUM_THREADS,sizeof(float));

	float *Y; // output
	Y  = (float*)calloc(Len,sizeof(float));

	// cpu timer
	//struct timespec start_cputimer, end_cputimer;
	//float cpuTime;
	//clock_gettime(CLOCK_MONOTONIC, &start_cputimer);



	// pthreads
	pthread_t threads[NUM_THREADS];
	void *status;
	pthread_attr_t attr;
	int rc;

	parform.len = Len;
	parform.norm = nsec;
	parform.denorm = dsec;
	parform.inputArray = Xarray;
	parform.outArray   = Yarray;

	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

	for(tid=0;tid<NUM_THREADS;tid++){
		rc = pthread_create(&threads[tid],&attr,par_iir, (void*)tid);	
		if(rc){
			printf("ERROR; return code from pthread_create() is %d\n", rc);
			exit(-1);	 
		}
	}

	for(tid=0;tid<NUM_THREADS;tid++) {
		pthread_join(threads[tid], &status);
	}

	// after joining, reduction on Yarray, NUM_THREADS(row) x Len(col)

	// pthread

	// reduction on Yarray (16xN)
	// save to Y
	rdtinfo.input  = Yarray;
	rdtinfo.output = Y;
	rdtinfo.length = Len;
	rdtinfo.X = Xarray;
	rdtinfo.c = c;

	for(tid=0;tid<NUM_THREADS;tid++){
		rc = pthread_create(&threads[tid],&attr,reduction, (void*)tid);	
		if(rc){
			printf("ERROR; return code from pthread_create() is %d\n", rc);
			exit(-1);	 
		}
	}
	pthread_attr_destroy(&attr);
	for(tid=0;tid<NUM_THREADS;tid++) {
		pthread_join(threads[tid], &status);
	}


	//clock_gettime(CLOCK_MONOTONIC, &end_cputimer);
	//cpuTime = timespecDiff(&end_cputimer,&start_cputimer)/(float)1000000000;
	//printf("cpu runtime = %f second \n", cpuTime);
	//printf("total = %f second \n", cpuTime);


	// another timer
	gettimeofday(&end,NULL);
	double t1 = start.tv_sec + start.tv_usec/1000000.0;
	double t2 = end.tv_sec + end.tv_usec/1000000.0;
	printf("total = %lf second \n", t2-t1);

	//for(i=0;i<Len;++i){
	//	printf("[%d] %f\n",i+1,Y[i]);	
	//}

	// release
	free(Xarray);
	free(Yarray);
	free(nsec);
	free(dsec);
	free(Y);

	pthread_exit(NULL);
}
