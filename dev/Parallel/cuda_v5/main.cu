// System includes
#include <stdio.h>
#include <string.h>
#include <assert.h>

#include <cuda_runtime.h>
//#include <helper_functions.h>
#include <helper_cuda.h>
#include "../../common/cpp_utils.h"


void form_coeffMatrix(float *coeff, int row, int col, float *nsec, float *dsec, int order);
void print_coeffMatrix(float *coeff, int row, int col);
void update_coeffMatrix(float *coeff, int row, int col, float *dsec, int order);

int main(int argc, char *argv[])
{
//	if(argc != 2){
//		printf("Specify the lenght of input, e.g. ./main len !\n");
//		exit(1);
//	}

	int order = 2;
	int i;

	float *nsec, *dsec;
	nsec = (float*) malloc(sizeof(float) * (order + 1)); // numerator
	dsec = (float*) malloc(sizeof(float) * order); // denominator

	for(i=0; i<(order+1); i++){
		nsec[i] = 0.11f;
	}
	for(i=0; i<order; i++){
		dsec[i] = 0.99f;
	}

	int row = order * 2;
	int col = row * 2;
	float *coeff; // row x col
	coeff = (float*) malloc (sizeof(float) * row * col);
	memset(coeff, 0, sizeof(float) * row * col);

	form_coeffMatrix(coeff, row, col, nsec, dsec, order);
	print_coeffMatrix(coeff, row, col);
	puts("\n\n");
	update_coeffMatrix(coeff, row, col, dsec, order);
	print_coeffMatrix(coeff, row, col);

	// SIMD execution


	free(nsec);
	free(dsec);

	return EXIT_SUCCESS;
}

void form_coeffMatrix(float *coeff, int row, int col, float *nsec, float *dsec, int order)
{
	int i, j, k;
	int start, end, width;
	width = order + 1 + order;
	for(i=0; i<row; i++){
		start = col - width - i;	
		end = start + width;
		k = 0;
		for(j=start; j<end; j++){
			if(k <(order + 1)){
				coeff[i * col + j] = nsec[k]; // numerator
			}else{
				coeff[i * col + j] = dsec[k-(order+1)];// denominator
			}		
			k = k + 1;
		}
	}
}

void print_coeffMatrix(float *coeff, int row, int col)
{
	int i, j;
	for(i=0; i<row; i++){
		for(j=0; j<col; j++){
			printf("%f\t", coeff[i*col + j]);
		}
		printf("\n");
	}
}


void update_coeffMatrix(float *coeff, int row, int col, float *dsec, int order)
{
	int i, ii;
	int j, k;
	float sum;
	for(ii=0; ii<col; ii++){ // each col
		// from 2nd row
		for(i=1; i<row; i++){
			// current row, how many coefficients need to be added?
			if(i<order){
				j = i;
			}else{
				j = order;
			}
			sum = 0.f;
			// add extra coefficients
			for(k=0; k<j; k++) {
				sum += dsec[k] * coeff[(i-1)*col + ii];
			}
			coeff[i*col + ii] += sum; 
		}
	}
}

