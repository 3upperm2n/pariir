% Parallel implementation of IIR Filter

%
clear
clc

%% step1 : use fdatool to design an IIR filter
%         Here, Butterworth bandpass is implemented, with Fs=16khz,
%         bandpass from 10k to 11k, bandstop1 950hz, bandstop2 1150hz
%
%         export SOS, G to workspace, save them to data_iir
load data_iir

%% step2: load data_iir
%         use sos2tf to produce Num/Denum coefficients for the transfer function
%         example,  [b, a] = sos2tf(SOS,G);
[b, a] = sos2tf(SOS,G);

%% step3: Assume input data is 32-element vector of 1
%        Output the result Y
X = ones(1,32);
Y = filter(b,a,X);

%% step4: implement the previous IIR parallely
%        reference to "https://github.com/burakbayramli/kod/tree/master/books/A_Course_DSP_Porat/MFILES"
%        for background, check this "http://www.ingelec.uns.edu.ar/pds2803/Materiales/LibrosPDF/Porat/TOC.htm"
%        [c,nsec,dsec] = tf2rpf(b,a)


[c,nsec,dsec] = tf2rpf(b,a);

Y_par = parallel(c,nsec,dsec,X);

%% step5: verify the cascaded and parallel implementation result
%        max(Y - Y_par) is  6.4673e-18, which is trivally small and can be considered to be zero



%% step6: save nsec, dsec, Y_par for c++
fid = fopen('nsec.txt', 'w');
fprintf(fid, '%d %d\n', size(nsec,1), size(nsec,2));
for i = 1:size(nsec,1)
    fprintf(fid, '%e %e\n', nsec(i,1), nsec(i,2));
end
fclose(fid);

fid = fopen('dsec.txt', 'w');
fprintf(fid, '%d %d\n', size(dsec,1), size(dsec,2));
for i = 1:size(dsec,1)
    fprintf(fid, '%e %e %e\n', dsec(i,1), dsec(i,2), dsec(i,3));
end
fclose(fid);

if size(Y_par,1) == 1
    Y_par = Y_par';
end

fid = fopen('c.txt', 'w');
fprintf(fid, '%e\n', c);
fclose(fid);

fid = fopen('output.txt', 'w');
fprintf(fid, '%d\n', length(Y_par));
fprintf(fid, '%e\n', Y_par(:,1));
fclose(fid);

