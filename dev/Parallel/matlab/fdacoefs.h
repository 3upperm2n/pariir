/*
 * Filter Coefficients (C Source) generated by the Filter Design and Analysis Tool
 *
 * Generated by MATLAB(R) 7.13 and the Signal Processing Toolbox 6.16.
 *
 * Generated on: 15-May-2013 13:05:46
 *
 */

/*
 * Discrete-Time IIR Filter (real)
 * -------------------------------
 * Filter Structure    : Direct-Form II, Second-Order Sections
 * Number of Sections  : 15
 * Stable              : Yes
 * Linear Phase        : No
 */

/* General type conversion for MATLAB generated C-code  */
#include "tmwtypes.h"
/* 
 * Expected path to tmwtypes.h 
 * /usr/local/matlab/r2011b/extern/include/tmwtypes.h 
 */
/*
 * Warning - Filter coefficients were truncated to fit specified data type.  
 *   The resulting response may not match generated theoretical response.
 *   Use the Filter Design & Analysis Tool to design accurate
 *   single-precision filter coefficients.
 */
#define MWSPT_NSEC 31
const int NL[MWSPT_NSEC] = { 1,3,1,3,1,3,1,3,1,3,1,3,1,3,1,3,1,3,1,3,1,3,1,3,1,3,1,3,1,3,1 };
const real32_T NUM[MWSPT_NSEC][3] = {
  {
    0.02049390785,              0,              0 
  },
  {
                1,              0,             -1 
  },
  {
    0.02049390785,              0,              0 
  },
  {
                1,              0,             -1 
  },
  {
    0.02040875889,              0,              0 
  },
  {
                1,              0,             -1 
  },
  {
    0.02040875889,              0,              0 
  },
  {
                1,              0,             -1 
  },
  {
    0.02033018321,              0,              0 
  },
  {
                1,              0,             -1 
  },
  {
    0.02033018321,              0,              0 
  },
  {
                1,              0,             -1 
  },
  {
    0.02026135102,              0,              0 
  },
  {
                1,              0,             -1 
  },
  {
    0.02026135102,              0,              0 
  },
  {
                1,              0,             -1 
  },
  {
     0.0202049464,              0,              0 
  },
  {
                1,              0,             -1 
  },
  {
     0.0202049464,              0,              0 
  },
  {
                1,              0,             -1 
  },
  {
     0.0201631058,              0,              0 
  },
  {
                1,              0,             -1 
  },
  {
     0.0201631058,              0,              0 
  },
  {
                1,              0,             -1 
  },
  {
    0.02013737522,              0,              0 
  },
  {
                1,              0,             -1 
  },
  {
    0.02013737522,              0,              0 
  },
  {
                1,              0,             -1 
  },
  {
    0.02012869343,              0,              0 
  },
  {
                1,              0,             -1 
  },
  {
                1,              0,              0 
  }
};
const int DL[MWSPT_NSEC] = { 1,3,1,3,1,3,1,3,1,3,1,3,1,3,1,3,1,3,1,3,1,3,1,3,1,3,1,3,1,3,1 };
const real32_T DEN[MWSPT_NSEC][3] = {
  {
                1,              0,              0 
  },
  {
                1,   -1.811522365,   0.9955168962 
  },
  {
                1,              0,              0 
  },
  {
                1,   -1.844562888,   0.9959161282 
  },
  {
                1,              0,              0 
  },
  {
                1,   -1.804410934,   0.9868294597 
  },
  {
                1,              0,              0 
  },
  {
                1,    -1.83655405,   0.9879490733 
  },
  {
                1,              0,              0 
  },
  {
                1,   -1.798670411,   0.9788545966 
  },
  {
                1,              0,              0 
  },
  {
                1,   -1.828397751,    0.980492115 
  },
  {
                1,              0,              0 
  },
  {
                1,    -1.79454267,   0.9719542265 
  },
  {
                1,              0,              0 
  },
  {
                1,   -1.820395827,   0.9738230109 
  },
  {
                1,              0,              0 
  },
  {
                1,   -1.792179227,   0.9664214253 
  },
  {
                1,              0,              0 
  },
  {
                1,   -1.812850356,   0.9681995511 
  },
  {
                1,              0,              0 
  },
  {
                1,   -1.791639209,   0.9624695778 
  },
  {
                1,              0,              0 
  },
  {
                1,   -1.806057572,   0.9638538957 
  },
  {
                1,              0,              0 
  },
  {
                1,   -1.792894483,   0.9602277875 
  },
  {
                1,              0,              0 
  },
  {
                1,   -1.800300598,   0.9609837532 
  },
  {
                1,              0,              0 
  },
  {
                1,   -1.795838714,   0.9597426057 
  },
  {
                1,              0,              0 
  }
};
