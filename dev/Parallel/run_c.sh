#!/bin/bash


# delete the previous record
if [ -f ./profile_c/len_$1.txt ];
then
  rm ./profile_c/len_$1.txt
fi

for (( i=1 ; i<=20; i = i+1 ))
do
./parIIR_c $1 >> ./profile_c/len_$1.txt 
done

./run_time.sh ./profile_c/len_$1.txt | cut -f1 | sort -nr | tail -1 >> ./profile_c/final_data.txt 
