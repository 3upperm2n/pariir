#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#include "../cpp_utils.h"

int main(int argc, char *argv[])
{
	if(argc!=2){
		printf("Wrong Usage!\nTo use program, type like $./PROGRAM DATA_LENGTH.\n");
		exit(1);
	}

	int i, k;
	int len;
	len = atol(argv[1]);

	// Input Parameters:
	// c: the free term of the filter
	// nsec, dsec: numerator and demoninator of second-order sections
	// x: input sequence
	// Output Parameters:
	// y: output sequence


	// input
	float *x;
	x = (float*) malloc(sizeof(float)*len);
	for(i=0; i<len; i++)
	{
		x[i] = 1.f;
	}

	// output
	float *y;
	y = (float*) malloc(sizeof(float)*len);

	int row = 32;

	// the first column of dsec is 1
	float *dsec;
	dsec = (float*) malloc(sizeof(float)*row*2);
	for(i=0;i<row*2;i++)
	{
		dsec[i] = 0.5;
	}

	float *nsec;
	nsec = (float*) malloc(sizeof(float)*row*2);
	for(i=0;i<row*2;i++)
	{
		nsec[i] = 0.2;
	}

	float c = 3.0;

	// internal state
	float *u = (float*) malloc(sizeof(float) * row * 2);
	memset(u, 0 , sizeof(float) * row * 2);

	float unew;

	struct timeval timer;
	tic(&timer);

	for(i=0; i<len; i++)
	{
		y[i] = c * x[i];

		for(k=0; k<row; k++)
		{
			unew = x[i] - (dsec[k*2] * u[k * 2] + dsec[k*2+1] * u[k * 2 + 1]);

			u[k*2 + 1] = u[k * 2];
			u[k*2] = unew;

			y[i] = y[i] + (u[k * 2] * nsec[k*2] + u[k*2 + 1] * nsec[k*2 + 1]);
		}
	}

	toc(&timer);

	//print1df(y, len);


	free(x);
	free(y);
	free(dsec);
	free(nsec);
	free(u);


/*

	// cpu timer
	//struct timespec start_cputimer, end_cputimer;
	//float cpuTime;
	//clock_gettime(CLOCK_MONOTONIC, &start_cputimer);
	struct timeval start,end;
	gettimeofday(&start,NULL);


	//clock_gettime(CLOCK_MONOTONIC, &end_cputimer);
	//cpuTime = timespecDiff(&end_cputimer,&start_cputimer)/(float)1000000000;
	//printf("cpu runtime = %f second \n", cpuTime);
	//printf("total = %f second \n", cpuTime);

	gettimeofday(&end,NULL);
	double t1 = start.tv_sec + start.tv_usec/1000000.0;
	double t2 = end.tv_sec + end.tv_usec/1000000.0;
	printf("total = %lf seconds \n", t2-t1);

	for(i=0;i<Len;++i){
		printf("[%d] %lf\n",i+1, Y[i]);
	}


	*/

	return 0;
}
