#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<time.h>
#include<sys/time.h>
#include<stdbool.h>
#include<pthread.h>

#include <CL/opencl.h>

#define NUM_THREADS 16
#define OCL_CHECK(x) oclcheck((x),__FILE__,__LINE__)

typedef struct
{
	float *input;
	float *output;
	float *denorm;
	unsigned int len;
} GEN_U;

GEN_U genU;

void printCompilerOutput(cl_program program, cl_device_id device) {

	cl_build_status build_status;
	clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_STATUS,
			sizeof(cl_build_status), &build_status, NULL);

	if(build_status == CL_SUCCESS) {
		printf("No compilation errors for this device\n");
		return;
	}

	size_t ret_val_size;
	clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG, 0, NULL,
			&ret_val_size);

	char* build_log = NULL;
	build_log = (char *)malloc(ret_val_size+1);
	if(build_log == NULL){
		perror("malloc");
		exit(1);
	}

	clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG,
			ret_val_size+1, build_log, NULL);
	build_log[ret_val_size] = '\0';

	printf("Build log:\n %s...\n", build_log);
}

//int64_t timespecDiff(struct timespec *timeA_p, struct timespec *timeB_p)
//{
//	return ((timeA_p->tv_sec * 1000000000) + timeA_p->tv_nsec) - ((timeB_p->tv_sec * 1000000000) + timeB_p->tv_nsec);
//}


void oclcheck(int err, const char *file, const int linenum)
{
	if(err != CL_SUCCESS){
		printf("Error! file %s: line %d", file, linenum);
		printf("\n");
		exit(-1);
	}
}

void printArray(float *A, int N)
{
	int i;
	for(i=0;i<N;i++)
	{
		//printf("%e ",A[i]);
		printf("%5d ",(int)A[i]);
	}
	printf("\n");
}




void *genMat(void *arg)
{
	int taskid;
	taskid = (int) arg;

	float tmp;

	//float u[2] = {0.f, 0.f};
	float u0 = 0.f;
	float u1 = 0.f;

	float *Mat;
	Mat = genU.output;

	float *X;
	X = genU.input;
	
	float *dsec;
	dsec = genU.denorm;

	unsigned int Len;
	Len = genU.len;

	unsigned int thread_start = Len*taskid;

	unsigned int i;
	for(i=0;i<Len;++i){
		tmp = X[thread_start+i] - u0*dsec[taskid*2] - u1*dsec[taskid*2+1];	
		// swap u0,u1
		Mat[i*32+2*taskid+1] = u1 = u0; 
		Mat[i*32+2*taskid] = u0 = tmp;	
	}

	pthread_exit(NULL);

}





int main(int argc, char *argv[])
{

	if(argc!=2){
		printf("Wrong Usage!\nTo use program, type like $./PROGRAM DATA_LENGTH.\n");
		exit(1);
	}

	struct timeval cpu_start,cpu_end;
	gettimeofday(&cpu_start,NULL);

	// read correct output
	unsigned int Len;
	Len = atol(argv[1]);
	//printf("Length of input = %d\n", Len);


	// read nominator
	unsigned int i;
	int nr,nl;

	nr = 16;
	nl = 2;
	//printf("nsec row=%d col=%d\n", nr, nl);

	float *nsec;
	nsec = (float*) malloc(sizeof(float)*nr*nl);
	for(i=0;i<32;i++){
		nsec[i] = 0.5;
	}

	// read c 
	float c = 3.0;

	// read denominator
	int dnr,dnl;

	dnr =16;
	dnl=2;
	//printf("dsec row=%d col=%d\n", dnr, dnl);

	float *dsec;
	dsec = (float*) malloc(sizeof(float)*dnr*dnl);
	for(i=0;i<32;i++){
		dsec[i] = 2.0;
	}

	// allocate input/output data 
	float *X;
	float *Y;

	X  = (float*) malloc(sizeof(float)*Len*16);  //  duplicate NUM_THREADS
	Y  = (float*) calloc(Len,sizeof(float));


	// need to be duplicate NUM_THREADS times
	// initialize input
	for(i=0;i<Len*16;i++){
		X[i] = 1.0;
	}

	int row = nr;

	float *u;	// buffer	
	u = (float*) calloc(row*2, sizeof(float));

	float *Mat; // intermediate data : here, 32(len) x 16(row) x 2(col) 
	Mat = (float*) calloc(Len*row*2,sizeof(float));

	//float unew;
	//int j;
	//int offset = row*2;

	// cpu timer
	//struct timespec start_cputimer, end_cputimer;
	//float cpuTime;
	// time CPU
	//clock_gettime(CLOCK_MONOTONIC, &start_cputimer);

	// pthread
	pthread_t threads[NUM_THREADS]; 
	pthread_attr_t attr;
	int rc;
	void *status;

	genU.output = Mat;
	genU.input = X;
	genU.denorm = dsec;
	genU.len = Len;

	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr,PTHREAD_CREATE_JOINABLE);
	
	int tid;
	for(tid=0;tid<NUM_THREADS;++tid){
		rc = pthread_create(&threads[tid], &attr, genMat, (void*)tid);
		if(rc){
			printf("ERROR; return code from pthread_create() is %d\n", rc);
			exit(-1);	 
		}
	}

	pthread_attr_destroy(&attr);

	for(tid=0;tid<NUM_THREADS;++tid) {
		pthread_join(threads[tid], &status);
	}


	// from here, we have the Mat[],  N x 32





	//clock_gettime(CLOCK_MONOTONIC, &end_cputimer);
	//cpuTime = timespecDiff(&end_cputimer,&start_cputimer)/(float)1000000000;



	// opencl
	cl_mem d_Mat; // Lenx16x2 :  need 32 intermediate data to merge into 1 final data
	cl_mem d_nsec; // 16x2 
	cl_mem d_Y; // Len 
	cl_mem d_X; // Len

	cl_platform_id cpPlatform;        // OpenCL platform
	cl_device_id device_id;           // device ID
	cl_context context;               // context
	cl_command_queue queue;           // command queue
	cl_program program;               // program
	cl_kernel kernel;                 // kernel

	// GPU timer
	//cl_ulong start;
	//cl_ulong end;
	//cl_event event;
	//cl_event events[6];
	//float gpuTime;

	size_t globalSize, localSize;
	cl_int err;
	localSize = 32; // 16*2 
	globalSize = Len * 32;

	unsigned int totalN = Len*32;

	// read kernel file
	char *fileName = "parIIR_ocl_kernel.cl";
	char *kernelSource;
	size_t size;
	FILE *fh = fopen(fileName, "rb");
	if(!fh) {
		printf("Error: Failed to open kernel file!\n");
		exit(1);
	}
	fseek(fh,0,SEEK_END);
	size=ftell(fh);
	fseek(fh,0,SEEK_SET);
	kernelSource = malloc(size+1);
	size_t result;
	result = fread(kernelSource,1,size,fh);
	if(result != size){ fputs("Reading error", stderr);exit(1);}
	kernelSource[size] = '\0';

	// Bind to platform
	err = clGetPlatformIDs(1, &cpPlatform, NULL);
	OCL_CHECK(err);

	// Get ID for the device
	err = clGetDeviceIDs(cpPlatform, CL_DEVICE_TYPE_GPU, 1, &device_id, NULL);
	OCL_CHECK(err);

	// Create a context  
	context = clCreateContext(0, 1, &device_id, NULL, NULL, &err);
	OCL_CHECK(err);

	// Create a command queue 
	//queue = clCreateCommandQueue(context, device_id, 0, &err);
	queue = clCreateCommandQueue(context, device_id,CL_QUEUE_PROFILING_ENABLE, &err);
	OCL_CHECK(err);

	// Create the compute program from the source buffer
	program = clCreateProgramWithSource(context, 1, (const char **) & kernelSource, NULL, &err);
	OCL_CHECK(err);

	// Build the program executable 
	// turn on optimization for kernel
	//err = clBuildProgram(program, 1, &device_id, NULL, NULL, NULL);
	char *options="-cl-mad-enable -cl-fast-relaxed-math -cl-no-signed-zeros -cl-unsafe-math-optimizations -cl-finite-math-only";

	//char options[1024];
	//sprintf(options,"%s",ptr);

	err = clBuildProgram(program, 1, &device_id, options, NULL, NULL);
	if(err != CL_SUCCESS)
		printCompilerOutput(program, device_id);
	OCL_CHECK(err);

	// Create the compute kernel in the program we wish to run
	kernel = clCreateKernel(program, "iir_paral", &err);
	OCL_CHECK(err);

	// Create the input and output arrays in device memory for our calculation
	d_Mat  	= clCreateBuffer(context, CL_MEM_READ_ONLY,  sizeof(float)*Len*row*2,  NULL, NULL);
	d_nsec 	= clCreateBuffer(context, CL_MEM_READ_ONLY,  sizeof(float)*row*2, 		NULL, NULL);
	d_Y  	= clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof(float)*Len,     	NULL, NULL); // final result
	d_X  	= clCreateBuffer(context, CL_MEM_READ_ONLY,  sizeof(float)*Len,     	NULL, NULL);

	// Write our data set into the input array in device memory
	//err = clEnqueueWriteBuffer(queue, d_Mat, CL_TRUE, 0, sizeof(float)*Len*row*2, Mat, 0, NULL, &events[0]);
	err = clEnqueueWriteBuffer(queue, d_Mat, CL_TRUE, 0, sizeof(float)*Len*row*2, Mat, 0, NULL, NULL);
	OCL_CHECK(err);

	//err = clEnqueueWriteBuffer(queue, d_nsec, CL_TRUE, 0, sizeof(float)*row*2, nsec, 0, NULL, &events[1]);
	err = clEnqueueWriteBuffer(queue, d_nsec, CL_TRUE, 0, sizeof(float)*row*2, nsec, 0, NULL, NULL);
	OCL_CHECK(err);

	//err = clEnqueueWriteBuffer(queue, d_Y, CL_TRUE, 0, sizeof(float)*Len, Y, 0, NULL, &events[2]);
	err = clEnqueueWriteBuffer(queue, d_Y, CL_TRUE, 0, sizeof(float)*Len, Y, 0, NULL, NULL);
	OCL_CHECK(err);

	//err = clEnqueueWriteBuffer(queue, d_X, CL_TRUE, 0, sizeof(float)*Len, X, 0, NULL, &events[3]);
	err = clEnqueueWriteBuffer(queue, d_X, CL_TRUE, 0, sizeof(float)*Len, X, 0, NULL, NULL);
	OCL_CHECK(err);

	// Set the arguments to our compute kernel
	err  = clSetKernelArg(kernel, 0, sizeof(cl_mem), &d_Mat);
	if(err != 0) { printf("%d\n",err); OCL_CHECK(err); exit(1);}
	err |= clSetKernelArg(kernel, 1, sizeof(cl_mem), &d_nsec);
	if(err != 0) { printf("%d\n",err); OCL_CHECK(err); exit(1);}
	err |= clSetKernelArg(kernel, 2, sizeof(cl_mem), &d_Y);
	if(err != 0) { printf("%d\n",err); OCL_CHECK(err); exit(1);}
	err |= clSetKernelArg(kernel, 3, sizeof(float), &c);
	if(err != 0) { printf("%d\n",err); OCL_CHECK(err); exit(1);}
	err |= clSetKernelArg(kernel, 4, sizeof(unsigned int), &Len);
	if(err != 0) { printf("%d\n",err); OCL_CHECK(err); exit(1);}
	err |= clSetKernelArg(kernel, 5, sizeof(int), &nr);
	if(err != 0) { printf("%d\n",err); OCL_CHECK(err); exit(1);}
	err |= clSetKernelArg(kernel, 6, sizeof(int), &nl);
	if(err != 0) { printf("%d\n",err); OCL_CHECK(err); exit(1);}
	err |= clSetKernelArg(kernel, 7, sizeof(cl_mem), &d_X);
	if(err != 0) { printf("%d\n",err); OCL_CHECK(err); exit(1);}
	err |= clSetKernelArg(kernel, 8, sizeof(float)*32, NULL); // shared memory
	if(err != 0) { printf("%d\n",err); OCL_CHECK(err); exit(1);}
	err |= clSetKernelArg(kernel, 9, sizeof(unsigned int), &totalN);
	if(err != 0) { printf("%d\n",err); OCL_CHECK(err); exit(1);}
	

	// Execute the kernel over the entire range of the data set  
	// err = clEnqueueNDRangeKernel(queue, kernel, 1, NULL, &globalSize, &localSize, 0, NULL, NULL);
	//err = clEnqueueNDRangeKernel(queue, kernel, 1, NULL, &globalSize, &localSize, 0, NULL, &events[4]);
	err = clEnqueueNDRangeKernel(queue, kernel, 1, NULL, &globalSize, &localSize, 0, NULL, NULL);
	OCL_CHECK(err);

	// Wait for the command queue to get serviced before reading back results
	clFinish(queue);

	// Read the results from the device
	//clEnqueueReadBuffer(queue, d_Y, CL_TRUE, 0, sizeof(float)*Len, Y, 0, NULL, &events[5]);
	clEnqueueReadBuffer(queue, d_Y, CL_TRUE, 0, sizeof(float)*Len, Y, 0, NULL, NULL);

	// gather event time
	//err = clWaitForEvents(1,&events[5]);
	//OCL_CHECK(err);

	//err = clGetEventProfilingInfo (events[0], CL_PROFILING_COMMAND_START, sizeof(cl_ulong), &start, NULL);
	//OCL_CHECK(err);

	//err = clGetEventProfilingInfo (events[5], CL_PROFILING_COMMAND_END, sizeof(cl_ulong), &end, NULL);
	//OCL_CHECK(err);

	//gpuTime = (double) (end-start)/1000000000.0;
	//printf("gpu runtime(data transfer + kernel) = %lf seconds\n", gpuTime);	

	gettimeofday(&cpu_end,NULL);
	double t1 = cpu_start.tv_sec + cpu_start.tv_usec/1000000.0;
	double t2 = cpu_end.tv_sec   + cpu_end.tv_usec/1000000.0;
	//printf("cpu(pthread) runtime = %lf seconds \n", t2-t1);

	printf("total = %lf seconds\n", t2-t1);

	for(i=0;i<Len;++i){
		printf("[%d] %lf\n",i+1, Y[i]);
	}



	//printf("GPU Result:\n");
	//printArray(Y,Len);

	//for(i=0;i<6;i++){
	//	clReleaseEvent(events[i]);
	//}

	// release OpenCL resources
	clReleaseMemObject(d_Mat);
	clReleaseMemObject(d_nsec);
	clReleaseMemObject(d_Y);
	clReleaseProgram(program);
	clReleaseKernel(kernel);
	clReleaseCommandQueue(queue);
	clReleaseContext(context);


	//release host memory
	free(kernelSource);
	free(X);
	free(Y);
	free(nsec);
	free(dsec);
	free(Mat);
	free(u);

	return 0;
}
