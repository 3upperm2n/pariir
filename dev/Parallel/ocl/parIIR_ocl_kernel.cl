//#pragma OPENCL EXTENSION cl_khr_fp64: enable
//#pragma OPENCL EXTENSION cl_amd_printf : enable

__kernel void iir_paral(__global float *Mat, __constant float *nsec, __global float *Y, float c, const unsigned int N, int row, int col, __global float *X, __local volatile float *sm, const unsigned int totalN) 
{
	//	__local float sm[32]; // 16x2
	size_t gid = get_global_id(0);
	size_t lid = get_local_id(0);
	size_t gpid = get_group_id(0); // Len
	size_t ls = get_local_size(0);

	// simulate with multiples of 32 to avoid the halo at the end of data 
	if(gid<totalN){ // Lenx32
		sm[lid] = Mat[gid]*nsec[lid];	
		barrier(CLK_LOCAL_MEM_FENCE); 

		if (ls	>=  32) { sm[lid] += sm[lid + 16]; }
		if (ls	>=  16) { sm[lid] += sm[lid +  8]; }
		if (ls 	>=   8) { sm[lid] += sm[lid +  4]; }
		if (ls	>=   4) { sm[lid] += sm[lid +  2]; }
		if (ls	>=   2) { sm[lid] += sm[lid +  1]; }

		if(lid == 0) Y[gpid]=sm[0] + c*X[gpid];
	}
}
