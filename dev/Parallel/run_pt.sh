#!/bin/bash


# delete the previous record
if [ -f ./profile_pt/len_$1.txt ];
then
  rm ./profile_pt/len_$1.txt
fi

for (( i=1 ; i<=20; i = i+1 ))
do
./parIIR_pt $1 >> ./profile_pt/len_$1.txt 
done

./run_time.sh ./profile_pt/len_$1.txt | cut -f1 | sort -nr | tail -1 >> ./profile_pt/final_data.txt 
