#!/bin/bash


# ./profile options, where options can be -c [c version] , -ocl [OpenCL version] , clean [remove profiling data]
# Usage:  ./profile -c
#         ./profile -ocl
#         ./clean


# read the command options
if [ $# -eq 0 ];
then
  echo "$0 : You need to specify options, such as -c, -ocl, -pt or clean."
  exit 1
fi

if [ $# -gt 1 ];
then
  echo "$0 : Too many options. Use -c , -ocl, -pt, clean."
  exit 1
fi

#-------------------------------
# Remove previous profiling data 
#-------------------------------
if test $1 = "clean";
then
  if [ -d "profile_opencl" ];
  then
    rm -rf "profile_opencl"
  fi

  if [ -d "profile_c" ];
  then
    rm -rf "profile_c"
  fi

  if [ -d "profile_pthread" ];
  then
    rm -rf "profile_pthread"
  fi

else

  let "opt = 0"
  #-----------------------
  # profile OpenCL version
  #-----------------------
  if test $1 = "-ocl";
  then
    let "opt++"
  
    if [ ! -d "./profile_opencl" ];
    then
      mkdir "profile_opencl"
    fi
  
    if [ -f ./profile_opencl/final_data.txt ];
    then 
      rm ./profile_opencl/final_data.txt 
    fi

    if [ -f ./profile_opencl/warmup.txt ];
    then 
      rm ./profile_opencl/warmup.txt 
    fi
  
    # warmup
	echo "Start warming up ..."
    for (( i = 1 ;  i <=10 ; i = i + 1 ))
    do
      ./parIIR_ocl 32 >> ./profile_opencl/warmup.txt
    done
	echo "Finish warming up."

    #for (( i = 64 ;  i <= 1048576 ; i = i*2 ))
    for (( i = 1048576 ;  i <= 2097152 ; i = i*2 ))
    do
	  echo "Run data length $i"
      ./run_ocl.sh $i
    done

	echo "Check the ./profile_opencl/final_data.txt"
  
  fi
  
  #-----------------------
  # profile C version
  #-----------------------
  if test $1 = "-c";
  then
    let "opt++"
    if [ ! -d "./profile_c" ];
    then
      mkdir "profile_c"
    fi
  
    if [ -f ./profile_c/final_data.txt ];
    then 
      rm ./profile_c/final_data.txt 
    fi

    for (( i = 64 ;  i <= 1048576 ; i = i*2 ))
    do
	  echo "Run data length $i"
      ./run_c.sh $i
    done
	echo "Check the ./profile_c/final_data.txt"
  fi

  #-----------------------
  # profile Pthread version
  #-----------------------
  if test $1 = "-pt";
  then
    let "opt++"
    if [ ! -d "./profile_pt" ];
    then
      mkdir "profile_pt"
    fi
  
    if [ -f ./profile_pt/final_data.txt ];
    then 
      rm ./profile_pt/final_data.txt 
    fi

    #for (( i = 64 ;  i <= 1048576 ; i = i*2 ))
    for (( i = 1048576 ;  i <= 2097152 ; i = i*2 ))
    do
	  echo "Run data length $i"
      ./run_pt.sh $i
    done

	echo "Check the ./profile_pt/final_data.txt"
  
  fi
  
  if test $opt -eq 0;
  then
    echo "$0 : Wrong arguments. It should be  -c , -ocl , -pt or clean."
    exit 1
  fi

fi





