#!/bin/bash


# delete the previous record
if [ -f ./profile_opencl/len_$1.txt ];
then
  rm ./profile_opencl/len_$1.txt
fi

for (( i=1 ; i<=20; i = i+1 ))
do
./parIIR_ocl $1 >> ./profile_opencl/len_$1.txt 
done

./run_time.sh ./profile_opencl/len_$1.txt | cut -f1 | sort -nr | tail -1 >> ./profile_opencl/final_data.txt 
#./extractdata.sh len_$1.txt | cut -f1 | sort -nr | head -20 
