#ifndef CPP_UTILS_H
#define CPP_UTILS_H

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/time.h>

/* Function list

	print1df
	print2df
	tic
	toc

*/
void print1df(float *a, int len)
{
	int j;
	for(j = 0; j < len; ++j){ 
		printf("%.5f ", a[j] );
	}
	printf("\n");

}

void print2df(float *a, int row, int col)
{
	int i,j;
	for(i = 0 ; i < row; ++i){
		for(j = 0; j < col; ++j){ 
			printf("%.5f ", a[i * col + j] );
		}
		printf("\n");
	}
	printf("\n");
}

int timeval_subtract(struct timeval *result, struct timeval *t2, struct timeval *t1)
{
	long int diff = (t2->tv_usec + 1000000 * t2->tv_sec) - (t1->tv_usec + 1000000 * t1->tv_sec);
	result->tv_sec = diff / 1000000;
	result->tv_usec = diff % 1000000;
	return (diff<0);
}

void tic(struct timeval *timer)
{
	gettimeofday(timer, NULL);
}

void toc(struct timeval *timer)
{
	struct timeval tv_end, tv_diff;
	gettimeofday(&tv_end, NULL);
	timeval_subtract(&tv_diff, &tv_end, timer);
	printf("ElapsedTime = %ld.%06ld (s)\n", tv_diff.tv_sec, tv_diff.tv_usec);
}

#endif
